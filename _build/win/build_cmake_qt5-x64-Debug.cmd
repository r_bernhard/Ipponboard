::@echo off
setlocal
call "C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Auxiliary\Build\vcvars64.bat" || exit /b %errorlevel%
call _create_env_cfg.cmd x64 qt5 || exit /b %errorlevel%
echo using QTDIR=%QTDIR%

set BASE_DIR=%~dp0..\..
call _create_versioninfo.cmd %BASE_DIR%\base

:: Create the QT resource file --> obsolet, will be done in cmake
::%QTDIR%\bin\rcc.exe -name ipponboard %BASE_DIR%\base\ipponboard.qrc -o %BASE_DIR%\base\qrc_ipponboard.cpp || exit /b %errorlevel%

:: make Debug Makefiles
cmake -S "..\_cmake_qt5" -B "..\_build_cmake" -G "Visual Studio 17 2022" -A x64 -DCMAKE_BUILD_TYPE=Debug

:: make tests 
::cmake --build ..\_build_cmake --config Release --target IpponboardTest
::pushd ..\_build_cmake\test\Debug
::IpponboardTest
::popd
::if errorlevel 1 exit /b 1

:: build Debug
::cmake -S "..\_cmake_qt5" -B "..\_build_cmake" -G "Visual Studio 17 2022" -A x64 -DCMAKE_BUILD_TYPE=Debug
::cmake --build ..\_build_cmake --config Debug --target Ipponboard_Debug

pause
