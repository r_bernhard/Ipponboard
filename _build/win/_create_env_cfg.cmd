::@echo off
::setlocal EnableDelayedExpansion

if "%1"=="" goto :missing_args
if "%2"=="" goto :missing_args
set ARCH=%1
set QT=%2

set LOCAL_CONFIG=_env_cfg.cmd
if exist "%LOCAL_CONFIG%" (
  goto :end
)

echo create file=%LOCAL_CONFIG%
echo @echo off>"%LOCAL_CONFIG%"

if %QT% == qt4 (
echo set QTDIR=C:\dev\tools\qt-4.8.7-%ARCH%-msvc2017>>"%LOCAL_CONFIG%"
echo set PATH=%%PATH%%;C:\dev\tools\qt-4.8.7-%ARCH%-msvc2017\bin;C:\dev\tools\jom_1_1_4>>"%LOCAL_CONFIG%"
echo set QMAKESPEC=C:\dev\tools\qt-4.8.7-x64-msvc2017\mkspecs\win32-msvc2017>>"%LOCAL_CONFIG%"
)

if %QT% == qt5 (
echo set QTDIR=C:\dev\tools\qt-5.15.2-%ARCH%>>"%LOCAL_CONFIG%"
echo set PATH=%%PATH%%;C:\dev\tools\qt-5.15.2-%ARCH%\bin;C:\dev\tools\jom_1_1_4>>"%LOCAL_CONFIG%"
echo set QMAKESPEC=C:\dev\tools\qt-5.15.2-x64\mkspecs\win32-msvc>>"%LOCAL_CONFIG%"
)

echo set BOOST_DIR=C:\dev\tools\boost_1_87_0>>"%LOCAL_CONFIG%"
echo set INNO_DIR="C:\Program Files (x86) (x86)\Inno Setup 6">>"%LOCAL_CONFIG%"
echo set REDIST_DIR_x86=C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Redist\MSVC\14.42.34433\x86\Microsoft.VC143.CRT>>"%LOCAL_CONFIG%"
echo set REDIST_DIR_x64=C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Redist\MSVC\14.42.34433\x64\Microsoft.VC143.CRT>>"%LOCAL_CONFIG%"
echo set REDIST_DIR_x86d=C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Redist\MSVC\14.42.34433\debug_nonredist\x86\Microsoft.VC143.DebugCRT>>"%LOCAL_CONFIG%"
echo set REDIST_DIR_x64d=C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Redist\MSVC\14.42.34433\debug_nonredist\x64\Microsoft.VC143.DebugCRT>>"%LOCAL_CONFIG%"
goto :end
 
:missing_args
echo Fehlende Argumente: _create_env_cfg.cmd x86/x64 qt4/qt5
exit /b 1

:end
call "%LOCAL_CONFIG%"
::echo using QTDIR=%QTDIR%
exit /b 0
