# Content

- [Fork of https://github.com/fmuecke/Ipponboard.git]
- [Why it's *so* cool!](#why-its-so-cool)
- [Getting started](#getting-started)

Ipponboard is a score board specially designed for Judo tournaments that features an innovative control via gamepad, 
mouse, or keyboard. The main goals in its development were good readability and easy, intuitive control.

![Screenshot](https://ipponboard.koe-judo.de/wp-content/uploads/2011/12/Ipponboard-the-judo-score-board-and-timer-300x169.jpg)

# Fork of https://github.com/fmuecke/Ipponboard.git
This is a fork of Florian Mückes great Ipponboard. 
I decided to use a fork to process pull requests independently and merge them back into a master. 
The commit and git-branch history should document all adjustments. If Florian likes some of these ideas, 
I would be happy if they were incorporated into the original project.

# Why it's *so* cool!

- Reliability, ease and straightforward in use
- Everything in view: clocks, scores, fighters, lists – as well as for the audience, coaches and the fighters
- Whole fight control can be done by a single person instead of three (clocks, list handling, scores)
- Thanks to the innovative gamepad control you can keep track of the fight instead of looking for your mous cursor

## Even more features

- Automated list handling for team matches (including PDF export of the results)
- Golden Score automatism
- Automatic addition of penalties and hold scores
- New clubs can be easily added with the club manager
- Customizable mat signal and label
- Customizable colors (instead of the default blue and white)

# Download Releases
You can download and install the latest version from [Download Releases](https://raspi4.merzweiler.de/nextcloud/s/BPDwDizoL2QZmLD). Login with "ippnBord".

# Getting started
You can build Ipponboard from source from [Gitlab Ipponboard](https://gitlab.com/r_bernhard/Ipponboard).

Basic requirements to get started:
- Computer/laptop with Windows, Redhat- or Debian-Linux operating system (primary view)
- A big computer or TV screen (secondary view)

More information about usage and configuration is described within the manual:
- [German manual](doc/manual-de.md)
- [English manual](doc/USER_MANUAL-EN.md)

# How much does it cost?
- Ipponboard can be used free of charge.

# Contributing
See [CONTRIBUTING.md](CONTRIBUTING.md) and [LICENSE.txt](LICENSE.txt)

# Contact
- source code repository: https://gitlab.com/r_bernhard/Ipponboard
