#include "../util/debug.h"
#include "MainWindowEncounter.h"
#include "ui_MainWindowEncounter.h"

#include "../core/edition_encounter/ControllerEncounter.h"
#include "../base/View.h"

#include <QSettings>

using namespace Ipponboard;

// ---------------------- Constructors/Destructors ----------------------

MainWindowEncounter::MainWindowEncounter(QWidget* parent)
    : MainWindowBase(parent)
    , m_pUi(new Ui::MainWindowEncounter)
    , m_pTableView_encounters_model(new TableModelEncounter(parent))
{
    TRACE(2, "MainWindowEncounter::MainWindowEncounter()");

    m_pUi->setupUi(this);
    m_pController = new ControllerEncounter();
}

MainWindowEncounter::~MainWindowEncounter()
{
    TRACE(2, "MainWindowEncounter::~MainWindowEncounter()");

    if (m_pTableView_encounters_model)
    {
        delete m_pTableView_encounters_model;
        m_pTableView_encounters_model = nullptr;
    }

    if (m_pController)
    {
        delete m_pController;
        m_pController = nullptr;
    }
}

// ---------------------- Overrides ----------------------

void MainWindowEncounter::Init()
{
    TRACE(2, "MainWindowEncounter::Init()");

    MainWindowBase::Init();

    m_pUi->tableView_encounters->setModel(m_pTableView_encounters_model);
    m_pUi->tableView_encounters->resizeColumnsToContents();
    // TODO: Fill the list: 1. Type in the values via the list, 2. Save the list, 3. Load the list after startup
    //TRACE[TableModelTeam.cpp:298]: TableModelTeam::setData()
    //TRACE[SimpleCsvFile.hpp:80]: WriteData(fileName=FightersTeam.csv)

    //m_pUi->actionAutoAdjustPoints->setChecked(m_pController->IsAutoAdjustPoints());

}

void MainWindowEncounter::UpdateGoldenScoreView()
{
    TRACE(2, "MainWindowEncounter::UpdateGoldenScoreView()");

    m_pUi->checkBox_golden_score->setEnabled(m_pController->GetRules()->IsOption_OpenEndGoldenScore());
    m_pUi->checkBox_golden_score->setChecked(m_pController->IsGoldenScore());
}

void MainWindowEncounter::write_specific_settings(QSettings& settings)
{
    TRACE(2, "MainWindowEncounter::write_specific_settings()");

    settings.beginGroup(EditionNameShort());
    {
        settings.remove("");
        settings.setValue(str_tag_LabelHome, m_pController->GetHomeLabel());
        settings.setValue(str_tag_LabelGuest, m_pController->GetGuestLabel());
    }
    settings.endGroup();
}

void MainWindowEncounter::read_specific_settings(QSettings& settings)
{
    TRACE(2, "MainWindowEncounter::read_specific_settings()");

    settings.beginGroup(EditionNameShort());
    {
        m_pController->SetLabels(
            settings.value(str_tag_LabelHome, tr("Home")).toString(),
            settings.value(str_tag_LabelGuest, tr("Guest")).toString());
    }
    settings.endGroup();
}

void MainWindowEncounter::attach_primary_view()
{
    TRACE(2, "MainWindowEncounter::attach_primary_view()");

    if (QWidget* pWidget = dynamic_cast<QWidget*>(m_pPrimaryView.get()))
        m_pUi->verticalLayout_3->insertWidget(0, pWidget, 0);
}

void MainWindowEncounter::retranslate_Ui()
{
    TRACE(2, "MainWindowEncounter::retranslate_Ui()");

    m_pUi->retranslateUi(this);
}

void MainWindowEncounter::ui_check_language_items()
{
    TRACE(2, "MainWindowEncounter::ui_check_language_items()");

    m_pUi->actionLang_Deutsch->setChecked("de" == m_Language);
    m_pUi->actionLang_English->setChecked("en" == m_Language);
    m_pUi->actionLang_Dutch->setChecked("nl" == m_Language);
}

void MainWindowEncounter::ui_check_rules_items()
{
    TRACE(2, "MainWindowEncounter::ui_check_rules_items()");
    // TODO
}

void MainWindowEncounter::ui_check_show_secondary_view(const bool checked) const
{
    TRACE(2, "MainWindowEncounter::ui_check_show_secondary_view(checked=%d)", checked);

    m_pUi->actionShow_SecondaryView->setChecked(checked);
}

void MainWindowEncounter::update_views()
{
    TRACE(2, "MainWindowEncounter::update_views()");

    MainWindowBase::update_views();

    const auto rules = m_pController->GetRules();
    m_pUi->label_usedRules->setText(rules->Name());
}


// ---------------------- Events ----------------------

void MainWindowEncounter::on_checkBox_golden_score_toggled(const bool toggled)
{
    TRACE(2, "MainWindowEncounter::on_checkBox_golden_score_toggled(toggled=%d)", toggled);

    m_pController->SetCurrentFightGoldenScore(toggled);
}

// ---------------------- Private methods ----------------------


