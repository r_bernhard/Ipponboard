#ifndef MAINWINDOWENCOUNTER_H
#define MAINWINDOWENCOUNTER_H

#include "../MainWindowBase.h"
#include "../core/edition_encounter/TableModelEncounter.h"

#include <memory>

//forwards
namespace Ui { class MainWindowEncounter; }

namespace Ipponboard
{
class MainWindowEncounter : public MainWindowBase
{
    Q_OBJECT
public:
    explicit MainWindowEncounter(QWidget* parent = nullptr);
    virtual ~MainWindowEncounter();

    virtual void Init() final;

    virtual EditionType Edition() const final			{ return EditionType::Encounter; }
    virtual const char* EditionName() const final		{ return "Encounter Edition"; }
    virtual const char* EditionNameShort() const final	{ return "Encounter"; }

protected:
    virtual void UpdateGoldenScoreView() final;
    virtual void write_specific_settings(QSettings& settings) final;
    virtual void read_specific_settings(QSettings& settings) final;
    virtual void attach_primary_view() final;
    virtual void retranslate_Ui() final;
    virtual void ui_check_language_items() final;
    virtual void ui_check_rules_items() final;
    virtual void ui_check_show_secondary_view(bool checked) const final;

    virtual void update_views() override;

private:
    std::unique_ptr<Ui::MainWindowEncounter> m_pUi;

    TableModelEncounter* m_pTableView_encounters_model;

private slots:
    void on_checkBox_golden_score_toggled(bool);
};
} // namespace Ipponboard
#endif // MAINWINDOWENCOUNTER_H
