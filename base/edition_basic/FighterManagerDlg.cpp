﻿// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#include "../util/debug.h"
#include "FighterManagerDlg.h"
#include "ui_FighterManagerDlg.h"

#include "FighterManager.h"
#include "../ClubManager.h"
#include "FightCategoryManager.h"
#include "../core/Fighter.h"

#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QRegExp>
#include <QPlainTextEdit>

using namespace Ipponboard;

//---------------------------------------------------------
FighterManagerDlg::FighterManagerDlg(
    FighterManager& fighterManager,
    ClubManager& clubManager,
    std::shared_ptr<FightCategoryMgr> pFightCategoryManager,
	QWidget* parent)
	: QDialog(parent)
	, ui(new Ui::FighterManagerDlg)
    , m_fighterManager(fighterManager)
    , m_clubManager(clubManager)
    , m_pFightCategoryManager(pFightCategoryManager)
	, m_filter()
	, m_formatStr(Ipponboard::FighterManager::DefaultExportFormat())
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::FighterManagerDlg()");
    ui->setupUi(this);

	// hide settings buton
	//TODO: improove!
	ui->pushButton_settings->hide();

	// set columns
	const auto headerItem = ui->treeWidget_fighters->headerItem();
	headerItem->setText(eColumn_club, tr("Club/Team"));
    headerItem->setText(eColumn_category, tr("Fight Category"));
	headerItem->setText(eColumn_weight, tr("Weight"));
	headerItem->setText(eColumn_firstName, tr("First Name"));
	headerItem->setText(eColumn_lastName, tr("Last Name"));

	// adjust column widths
	ui->treeWidget_fighters->setColumnWidth(eColumn_club, 150);
    ui->treeWidget_fighters->setColumnWidth(eColumn_category, 60);
	ui->treeWidget_fighters->setColumnWidth(eColumn_weight, 50);
	ui->treeWidget_fighters->setColumnWidth(eColumn_firstName, 100);
	ui->treeWidget_fighters->setColumnWidth(eColumn_lastName, 100);
#ifdef __QT4__
    ui->treeWidget_fighters->header()->setResizeMode(eColumn_firstName, QHeaderView::Stretch);
    ui->treeWidget_fighters->header()->setResizeMode(eColumn_lastName, QHeaderView::Stretch);
#else
    ui->treeWidget_fighters->header()->setSectionResizeMode(eColumn_firstName, QHeaderView::Stretch);
    ui->treeWidget_fighters->header()->setSectionResizeMode(eColumn_lastName, QHeaderView::Stretch);
#endif

	populate_view();
}

//---------------------------------------------------------
FighterManagerDlg::~FighterManagerDlg()
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::~FighterManagerDlg()");
    delete ui;
}

//---------------------------------------------------------
void FighterManagerDlg::SetFilter(FighterManagerDlg::EColumn column, const QString& value)
//---------------------------------------------------------
{
    TRACE(2, "FighterManager::SetFilter()");
    if (column < eColumn_MAX)
	{
		m_filter = std::make_pair(column, value);
		ui->treeWidget_fighters->hideColumn(m_filter.first);

		populate_view();
	}
}

//---------------------------------------------------------
void FighterManagerDlg::changeEvent(QEvent* e)
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::changeEvent(e=%s)", DebugHelpers::QEventToString(e).toUtf8().data());
    QDialog::changeEvent(e);

	switch (e->type())
	{
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;

	default:
		break;
	}
}

//---------------------------------------------------------
void FighterManagerDlg::init_fighter_dialog(const AddFighterDlg& dlg) const
//---------------------------------------------------------
{
    // Fill the club combobox
    QStringList clubs;

    if (m_filter.first == eColumn_club)
        clubs.append(m_filter.second);
    else
        clubs = m_clubManager.GetClubNames();

    dlg.SetClubs(clubs);

    // Fill the category combobox
    QStringList categories;

    if (m_filter.first == eColumn_category)
        categories.append(m_filter.second);
    else
        categories = m_pFightCategoryManager->GetCategoryNames();

    dlg.SetCategories(categories);
}

//---------------------------------------------------------
void FighterManagerDlg::on_pushButton_add_pressed()
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::on_pushButton_add_pressed()");

    AddFighterDlg dlg(m_pFightCategoryManager, this);
    init_fighter_dialog(dlg);

    // now open the dialog
    if (dlg.exec() == QDialog::Rejected)
		return;

    const Fighter fighter = dlg.GetFighter();
    m_fighterManager.m_fighters.insert(fighter);

	QStringList contents;

	for (int i = 0; i < eColumn_MAX; ++i) contents.append("");

    contents[eColumn_club] = fighter.GetClub();
    contents[eColumn_category] = fighter.GetCategory();
    contents[eColumn_weight] = fighter.GetWeight();
    contents[eColumn_firstName] = fighter.GetFirstName();
    contents[eColumn_lastName] = fighter.GetLastName();

    QTreeWidgetItem* pItem = new QTreeWidgetItem(contents, QTreeWidgetItem::UserType);
    //pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
	ui->treeWidget_fighters->addTopLevelItem(pItem);
}

//---------------------------------------------------------
void FighterManagerDlg::on_treeWidget_fighters_itemDoubleClicked(const QTreeWidgetItem* pItem, const int column)
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::on_treeWidget_fighters_itemDoubleClicked(column=%i)", column);

    if (pItem)
    {
        AddFighterDlg dlg(m_pFightCategoryManager, this);
        init_fighter_dialog(dlg);

        Fighter old_fighter(pItem->text(eColumn_firstName) , pItem->text(eColumn_lastName));
        old_fighter.SetClub(pItem->text(eColumn_club));
        old_fighter.SetWeight(pItem->text(eColumn_weight));
        old_fighter.SetCategory(pItem->text(eColumn_category));
        dlg.SetFighter(old_fighter);

        // now open the dialog
        if (dlg.exec() == QDialog::Rejected)
            return;

        if (m_fighterManager.RemoveFighter(old_fighter))
        {
            const Fighter fighter = dlg.GetFighter();
            m_fighterManager.m_fighters.insert(fighter);
            populate_view();
        }
    }
}

//---------------------------------------------------------
void FighterManagerDlg::populate_view()
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::populate_view()");
    ui->treeWidget_fighters->clear();

	const bool hasFilter = !m_filter.second.isEmpty();

	if (hasFilter)
	{
		const QString filterInfo = QString("%0: %1").arg(
									   ui->treeWidget_fighters->headerItem()->text(m_filter.first),
									   m_filter.second);

		ui->label_filterinfo->setText(filterInfo);
		ui->label_filterinfo->show();
	}
	else
	{
		ui->label_filterinfo->hide();
	}

    std::for_each(begin(m_fighterManager.m_fighters), end(m_fighterManager.m_fighters),
				  [&](Ipponboard::Fighter const & f)
	{
		bool skipItem = true;

		if (hasFilter)
		{
			switch (m_filter.first)
			{
			case eColumn_firstName:
                if (m_filter.second == f.GetFirstName())
					skipItem = false;

				break;

			case eColumn_lastName:
                if (m_filter.second == f.GetLastName())
					skipItem = false;

				break;

			case eColumn_club:
                if (m_filter.second == f.GetClub())
					skipItem = false;

				break;

			case eColumn_weight:
                if (m_filter.second == f.GetWeight())
					skipItem = false;

				break;

            case eColumn_category:
                if (m_filter.second == f.GetCategory())
                    skipItem = false;
                break;

			default:
				break;
			}
		}
		else
		{
			skipItem = false;
		}

		if (!skipItem)
		{
			QStringList contents;

			for (int i = 0; i < eColumn_MAX; ++i) contents.append("");

            contents[eColumn_club] = f.GetClub();
            contents[eColumn_category] = f.GetCategory();
            contents[eColumn_weight] = f.GetWeight();
            contents[eColumn_firstName] = f.GetFirstName();
            contents[eColumn_lastName] = f.GetLastName();

            QTreeWidgetItem* pItem = new QTreeWidgetItem(contents, QTreeWidgetItem::UserType);
            //pItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
			ui->treeWidget_fighters->addTopLevelItem(pItem);
		}
	});
}

//---------------------------------------------------------
void FighterManagerDlg::on_pushButton_import_pressed()
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::on_pushButton_import_pressed()");

    const QString fileName = QFileDialog::getOpenFileName(this,
							 tr("Select CSV file with fighters"),
							 QCoreApplication::applicationDirPath(),
							 tr("CSV files (*.csv);;Text files (*.txt)"), nullptr, QFileDialog::ReadOnly);

	if (!fileName.isEmpty())
	{
		//TODO: make this right
		on_pushButton_settings_pressed();

		if (QString errorMsg; m_fighterManager.ImportFighters(fileName, m_formatStr, errorMsg))
		{
			QMessageBox::information(
				this,
				QCoreApplication::applicationName(),
				errorMsg);

            //TODO?: fill_suitable_fighters(m_pUi->comboBox_weight->currentText());
		}
		else
		{
			QMessageBox::critical(
				this,
				QCoreApplication::applicationName(),
				errorMsg);
		}
	}

	populate_view();
}

//---------------------------------------------------------
void FighterManagerDlg::on_pushButton_export_pressed()
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::on_pushButton_export_pressed()");

    const QString fileName = QFileDialog::getSaveFileName(this,
							 tr("Name CSV file to store fighters in"),
							 QCoreApplication::applicationDirPath(),
							 tr("CSV files (*.csv);;Text files (*.txt)"));

	if (!fileName.isEmpty())
	{
		//TODO: make this right
		on_pushButton_settings_pressed();

		if (QString errorMsg; m_fighterManager.ExportFighters(fileName, m_formatStr, errorMsg))
		{
			QMessageBox::information(
				this,
				QCoreApplication::applicationName(),
				errorMsg);
		}
		else
		{
			QMessageBox::critical(
				this,
				QCoreApplication::applicationName(),
				errorMsg);
		}
	}
}

//---------------------------------------------------------
void FighterManagerDlg::on_pushButton_remove_pressed()
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::on_pushButton_remove_pressed()");

    QList<QTreeWidgetItem*> selectedItems = ui->treeWidget_fighters->selectedItems();
    for (QTreeWidgetItem* pItem : selectedItems)
	{
        Ipponboard::Fighter currentFighter(
			pItem->text(eColumn_firstName),
			pItem->text(eColumn_lastName));
        currentFighter.SetClub(pItem->text(eColumn_club));
        currentFighter.SetWeight(pItem->text(eColumn_weight));
        currentFighter.SetCategory(pItem->text(eColumn_category));

        ui->treeWidget_fighters->takeTopLevelItem(ui->treeWidget_fighters->indexOfTopLevelItem(pItem));

        m_fighterManager.RemoveFighter(currentFighter);

		delete pItem;
	}
}

//---------------------------------------------------------
void FighterManagerDlg::on_treeWidget_fighters_itemChanged(
	QTreeWidgetItem* pItem, const int column)
//---------------------------------------------------------
{
    TRACE(2, "FighterManagerDlg::on_treeWidget_fighters_itemChanged()");
    if (pItem)
	{
		QString firstName = pItem->text(eColumn_firstName);
		QString lastName = pItem->text(eColumn_lastName);
		QString club = pItem->text(eColumn_club);
		QString weight = pItem->text(eColumn_weight);
        QString category = pItem->text(eColumn_category);

		Ipponboard::Fighter changedFighter(firstName, lastName);
        changedFighter.SetClub(club);
        changedFighter.SetWeight(weight);
        changedFighter.SetCategory(category);

		qDebug("enum value: %i", column);

		switch (column)
		{
		case eColumn_firstName:
			firstName = m_tmpData;
			break;

		case eColumn_lastName:
			lastName = m_tmpData;
			break;

		case eColumn_club:
			club = m_tmpData;
			break;

		case eColumn_weight:
			weight = m_tmpData;
			break;

        case eColumn_category:
            category = m_tmpData;
            break;

		default:
			qDebug("ERROR: invalid enum value: %i", column);
			break;
		}

		Ipponboard::Fighter originalFighter(firstName, lastName);
        originalFighter.SetClub(club);
        originalFighter.SetWeight(weight);
        originalFighter.SetCategory(category);

        if (!m_fighterManager.RemoveFighter(originalFighter))
		{
			qDebug("error: original fighter not found!");
		}

        if (!m_fighterManager.AddFighter(changedFighter))
		{
			ui->treeWidget_fighters->takeTopLevelItem(
				ui->treeWidget_fighters->indexOfTopLevelItem(pItem));

			// due to duplicate entry
			qDebug("removed changed entry due to duplicate: %s %s",
                   changedFighter.GetFirstName().toLatin1().data(),
                   changedFighter.GetLastName().toLatin1().data());
		}
	}
}

void FighterManagerDlg::on_treeWidget_fighters_itemClicked(const QTreeWidgetItem* item, const int column)
{
    TRACE(2, "FighterManagerDlg::on_treeWidget_fighters_itemClicked()");
    m_tmpData = item->text(column);
	qDebug("data: %s", m_tmpData.toLatin1().data());
}

void FighterManagerDlg::on_pushButton_settings_pressed()
{
    TRACE(2, "FighterManagerDlg::on_pushButton_settings_pressed()");
    bool ok(false);
	const QString dlgTitle = tr("Specify import/export format");
	const QString dlgMsg = tr("Use valid specifiers and some kind of separator (;,:|/ etc.)"
						"\nValid specifiers are: %1")
					 .arg(Ipponboard::FighterManager::GetSpecifiererDescription());

	QString data = QInputDialog::getText(this,
										 dlgTitle,
										 dlgMsg,
										 QLineEdit::Normal,
										 m_formatStr,
										 &ok);

	QString separator;
	bool isValidSeparator = Ipponboard::FighterManager::DetermineSeparator(data, separator);

	QStringList dataParts;

	if (isValidSeparator)
	{
		dataParts = data.split(separator);
	}

	// at least 3 parts must be set (first, last, ...)
	while (ok && (!isValidSeparator || dataParts.size() < 3))
	{
		QMessageBox::critical(this,
							  tr(""),
							  tr("Invalid format. Please correct your input."));

		data = QInputDialog::getText(this,
									 dlgTitle,
									 dlgMsg,
									 QLineEdit::Normal,
									 data,
									 &ok);
		isValidSeparator =
			Ipponboard::FighterManager::DetermineSeparator(data, separator);

		if (isValidSeparator)
		{
			dataParts = data.split(separator);
		}
	}

	if (ok && isValidSeparator)
	{
		m_formatStr = data;
	}
}

