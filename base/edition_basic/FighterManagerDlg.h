// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef BASE_FIGHTERMANAGERDLG_H
#define BASE_FIGHTERMANAGERDLG_H

#include <QDialog>
#include <QString>
#include <QTreeWidgetItem>
#include "AddFighterDlg.h"

// forwards
class QListWidgetItem;

namespace Ui { class FighterManagerDlg; }

namespace Ipponboard
{
// forwards
class Fighter;
class FighterManager;
class ClubManager;
class FightCategoryMgr;

class FighterManagerDlg : public QDialog
{
	Q_OBJECT

public:
	enum EColumn
	{
        eColumn_firstName,
		eColumn_lastName,
        eColumn_club,
        eColumn_weight,
        eColumn_category,
        eColumn_MAX
	};

	explicit FighterManagerDlg(
        FighterManager& FighterManager,
        ClubManager& clubManager,
        std::shared_ptr<FightCategoryMgr> pFightCategoryManager,
		QWidget* parent = nullptr);

	virtual ~FighterManagerDlg();

	void SetFilter(EColumn column, QString const& value);

protected:
	void changeEvent(QEvent* e);

private slots:
	void on_treeWidget_fighters_itemChanged(QTreeWidgetItem* item, int column);
	void on_treeWidget_fighters_itemClicked(const QTreeWidgetItem* item, int column);
    void on_treeWidget_fighters_itemDoubleClicked(const QTreeWidgetItem *item, int column);
    void on_pushButton_import_pressed();
	void on_pushButton_export_pressed();
	//void on_buttonBox_rejected();
	//void on_buttonBox_accepted();
	void on_pushButton_remove_pressed();
	void on_pushButton_add_pressed();
	void on_pushButton_settings_pressed();


private:
	void populate_view();
    void init_fighter_dialog(const AddFighterDlg& dlg) const;

	Ui::FighterManagerDlg* ui;
    FighterManager& m_fighterManager;
    ClubManager& m_clubManager;
    std::shared_ptr<FightCategoryMgr> m_pFightCategoryManager;
	QString m_tmpData;
	std::pair<EColumn, QString> m_filter;
	QString m_formatStr;
};
} // namespace Ipponboard
#endif // BASE_FIGHTERMANAGERDLG_H
