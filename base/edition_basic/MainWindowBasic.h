// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef BASIC_EDITION_MAINWINDOW_H_
#define BASIC_EDITION_MAINWINDOW_H_

#include "../MainWindowBase.h"
#include "../util/helpers.hpp"
#include <memory>

#include "FighterManager.h"
#include "../ClubManager.h"
#include "FightCategoryManager.h"

// forwards

namespace Ui { class MainWindowBasic; }

namespace Ipponboard
{
class MainWindowBasic : public MainWindowBase
{
	Q_OBJECT
public:
	explicit MainWindowBasic(QWidget* parent = nullptr);
	virtual ~MainWindowBasic();

	virtual void Init() final;

    virtual EditionType Edition() const final			{ return EditionType::Basic; }
    virtual const char* EditionName() const final		{ return "Basic Edition"; }
    virtual const char* EditionNameShort() const final	{ return "Basic"; }

protected:
	virtual void UpdateGoldenScoreView() final;
	//virtual void changeEvent(QEvent* e) override;
	//virtual void closeEvent(QCloseEvent* event) override;
	//virtual void keyPressEvent(QKeyEvent* event) override;
	virtual void write_specific_settings(QSettings& settings) final;
	virtual void read_specific_settings(QSettings& settings) final;
	virtual void update_statebar() override;
	virtual void attach_primary_view() final;
	virtual void retranslate_Ui() final;
	virtual void ui_check_language_items() final;
	virtual void ui_check_rules_items() final;
	virtual void ui_check_show_secondary_view(bool checked) const final;
	virtual void ui_update_used_options();

private:
	/* specific private methods */
	void load_fighters();
	void save_fighters();
    void fill_suitable_fighters(const QString& weight, const FightCategory& category);
	void update_fighters(const QString& s);

protected slots:
	//virtual bool EvaluateSpecificInput(FMlib::Gamepad const* pGamepad) override;

	/* specific private slots */
    void on_actionManageFighters_triggered();
    void on_actionManageFightCategories_triggered();
	void on_comboBox_weight_currentIndexChanged(const QString&);
    void on_comboBox_name_second_currentIndexChanged(const QString&);
    void on_comboBox_name_first_currentIndexChanged(const QString&);
	void on_checkBox_golden_score_clicked(bool checked);
	void on_comboBox_fight_category_currentIndexChanged(const QString&);

private slots:
    void on_actionAutoAdjustPoints_toggled(bool checked) override;
	void on_actionViewInfoBar_toggled(bool checked) const;
	void on_toolButton_viewSecondaryScreen_toggled();
	virtual void closeEvent(QCloseEvent*) override;

private:
	/* member */
    std::unique_ptr<Ui::MainWindowBasic> m_pUi;
    std::shared_ptr<FightCategoryMgr> m_pCategoryManager;
	QStringList m_CurrentFighterNames;
    FighterManager m_fighterManager;
    ClubManager m_clubManager;
};
} // namespace Ipponboard

#endif  // BASIC_EDITION_MAINWINDOWBASIC_H_
