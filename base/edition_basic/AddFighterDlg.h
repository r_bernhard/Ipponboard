// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.


#ifndef ADDFIGHTERDLG_H
#define ADDFIGHTERDLG_H

#include <QDialog>
#include <memory>

namespace Ui { class AddFighterDlg; }

namespace Ipponboard
{
class Fighter;
class FightCategoryMgr;

class AddFighterDlg : public QDialog
{
	Q_OBJECT

public:
    explicit AddFighterDlg(
        std::shared_ptr<FightCategoryMgr> pFightCategoryManager,
        QWidget* parent = nullptr);

    virtual ~AddFighterDlg();

	void SetClubs(const QStringList& clubs) const;
    void SetCategories(const QStringList& categories) const;
    void SetWeights(const QStringList& weights) const;

    Fighter GetFighter() const;
    void SetFighter(const Fighter& fighter) const;

private slots:
    void on_comboBox_category_currentIndexChanged(const QString &categoryName);

private:
	Ui::AddFighterDlg* ui;
    std::shared_ptr<FightCategoryMgr> m_pFightCategoryManager;
};
} // namespace Ipponboard
#endif // ADDFIGHTERDLG_H
