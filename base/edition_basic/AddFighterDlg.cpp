// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#include "../util/debug.h"
#include "AddFighterDlg.h"
#include "FightCategoryManager.h"
#include "ui_AddFighterDlg.h"
#include "../core/Fighter.h"
#include "../core/edition_basic/FightCategory.h"

using namespace Ipponboard;

AddFighterDlg::AddFighterDlg(
    std::shared_ptr<FightCategoryMgr> pFightCategoryManager,
    QWidget* parent)
    : QDialog(parent)
    ,ui(new Ui::AddFighterDlg)
    , m_pFightCategoryManager(pFightCategoryManager)
{
    TRACE(2, "AddFighterDlg::AddFighterDlg(parent=%s)", parent->objectName().toUtf8().data());
    ui->setupUi(this);
}

AddFighterDlg::~AddFighterDlg()
{
    TRACE(2, "AddFighterDlg::~AddFighterDlg()");
    delete ui;
}

void AddFighterDlg::SetClubs(const QStringList& clubs) const
{
    TRACE(2, "AddFighterDlg::SetClubs()");

    ui->comboBox_club->addItems(clubs);
	ui->comboBox_club->setEnabled(clubs.size() > 1);
}

void AddFighterDlg::SetCategories(const QStringList& categories) const
{
    TRACE(2, "AddFighterDlg::SetCategories()");

    ui->comboBox_category->addItems(categories);
    ui->comboBox_category->setEnabled(categories.size() > 1);
}

void AddFighterDlg::SetWeights(const QStringList& weights) const
{
    TRACE(2, "AddFighterDlg::SetWeights()");

    ui->comboBox_weight->addItems(weights);
    ui->comboBox_weight->setEnabled(weights.size() > 1);
}

Fighter AddFighterDlg::GetFighter() const
{
    TRACE(2, "AddFighterDlg::GetFighter()");

    Fighter f(ui->lineEdit_firstname->text(), ui->lineEdit_lastname->text());
    f.SetClub(ui->comboBox_club->currentText());
    f.SetWeight(ui->comboBox_weight->currentText());
    f.SetCategory(ui->comboBox_category->currentText());

	return f;
}

void AddFighterDlg::SetFighter(const Fighter& fighter) const
{
    TRACE(2, "AddFighterDlg::SetFighter()");

    ui->lineEdit_firstname->setText(fighter.GetFirstName());
    ui->lineEdit_lastname->setText(fighter.GetLastName());

    int index = ui->comboBox_club->findText(fighter.GetClub());
    if ( index != -1 )
        ui->comboBox_club->setCurrentIndex(index);

    index = ui->comboBox_weight->findText(fighter.GetWeight());
    if ( index != -1 )
        ui->comboBox_weight->setCurrentIndex(index);

    index = ui->comboBox_category->findText(fighter.GetCategory());
    if ( index != -1 )
        ui->comboBox_category->setCurrentIndex(index);
}

void AddFighterDlg::on_comboBox_category_currentIndexChanged(const QString &categoryName)
{
    TRACE(2, "AddFighterDlg::on_comboBox_category_currentIndexChanged(categoryName=%s)", categoryName.toUtf8().data());

    FightCategory category(categoryName);
    m_pFightCategoryManager->GetCategory(categoryName, category);

    // add weights
    ui->comboBox_weight->clear();
    ui->comboBox_weight->addItems(category.GetWeightsList());
}

