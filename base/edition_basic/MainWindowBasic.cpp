// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#include "../util/debug.h"
#include "MainWindowBasic.h"
#include "ui_MainWindowBasic.h"

#include "FightCategoryManagerDlg.h"
#include "FighterManagerDlg.h"
#include "../core/edition_basic/ControllerBasic.h"
#include "../core/Fighter.h"
#include "../View.h"

#ifdef _WITH_GAMEPAD_
#include "../core/GamepadConfig.h"
#include "../gamepad/gamepad.h"
#endif
#include "../util/path_helpers.h"

#include <QColorDialog>
#include <QComboBox>
#include <QCompleter>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QDir>
#include <QFileDialog>
#include <QFontDialog>
#include <QInputDialog>
#include <QMenu>
#include <QMessageBox>
#include <QSettings>
#include <QTimer>
#include <QUrl>

#ifdef _WITH_GAMEPAD_
using namespace FMlib;
#endif
using namespace Ipponboard;

namespace StrTags { static const char* const edition = "Basic Edition"; }

MainWindowBasic::MainWindowBasic(QWidget* parent)
	: MainWindowBase(parent)
    , m_pUi(new Ui::MainWindowBasic)
    , m_pCategoryManager()
    , m_fighterManager()
    , m_clubManager()
{
    TRACE(2, "MainWindowBasic::MainWindowBasic()");

    m_pUi->setupUi(this);
    m_pController = new ControllerBasic();
}

MainWindowBasic::~MainWindowBasic()
{
    TRACE(2, "MainWindowBasic::~MainWindowBasic()");

    if (m_pController)
    {
        delete m_pController;
        m_pController = nullptr;
    }
}

void MainWindowBasic::Init()
{
    TRACE(2, "MainWindowBasic::Init()");

	MainWindowBase::Init();

    // At first load the fighter pool
    load_fighters();

    // Then load the fight categories and fill the right combobox with this values
    m_pCategoryManager.reset(new FightCategoryMgr());
    for (int i(0); i < (int)m_pCategoryManager->CategoryCount(); ++i)
	{
		FightCategory t("");
		m_pCategoryManager->GetCategory(i, t);
        m_pUi->comboBox_fight_category->addItem(t.ToString());
	}

	// trigger tournament class combobox update
    on_comboBox_fight_category_currentIndexChanged(m_pUi->comboBox_fight_category->currentText());

    m_pUi->actionAutoAdjustPoints->setChecked(m_pController->IsAutoAdjustPoints());
}

void MainWindowBasic::on_actionManageFightCategories_triggered()
{
    TRACE(2, "MainWindowBasic::on_actionManageFightCategories_triggered()");

    //save categories before editing
    m_pCategoryManager->SaveCategories();

    if (FightCategoryManagerDlg dlg(m_pCategoryManager, this); QDialog::Accepted == dlg.exec())
    {
        m_pCategoryManager->SaveCategories();

        QString currentClass = m_pUi->comboBox_fight_category->currentText();

        m_pUi->comboBox_fight_category->clear();

		for (int i(0); i < (int)m_pCategoryManager->CategoryCount(); ++i)
		{
			FightCategory t("");
			m_pCategoryManager->GetCategory(i, t);
            m_pUi->comboBox_fight_category->addItem(t.ToString());
		}

        int index = m_pUi->comboBox_fight_category->findText(currentClass);

		if (-1 == index)
		{
			index = 0;
            currentClass = m_pUi->comboBox_fight_category->itemText(index);
		}

        m_pUi->comboBox_fight_category->setCurrentIndex(index);
        on_comboBox_fight_category_currentIndexChanged(currentClass);
	}
    else
    {
        //load old categories to discard changes
        m_pCategoryManager->LoadCategories();
    }
}

void MainWindowBasic::on_actionManageFighters_triggered()
{
    TRACE(2, "MainWindowBasic::on_actionManageFighters_triggered()");

    FighterManagerDlg dlg(m_fighterManager, m_clubManager, m_pCategoryManager, this);
    dlg.exec();

    // populate the first and second fighter combobox of the primary view indirect by "changing" the weight
    on_comboBox_weight_currentIndexChanged(m_pUi->comboBox_weight->currentText());
}

void MainWindowBasic::on_comboBox_weight_currentIndexChanged(const QString& weight)
{
    TRACE(2, "MainWindowBasic::on_comboBox_weight_currentIndexChanged(weight=%s)", weight.toUtf8().data());

    const QString category_name = m_pUi->comboBox_fight_category->currentText();
    const FightCategory category(category_name);

    fill_suitable_fighters(weight, category);

    m_pPrimaryView->SetWeight(weight);
    m_pSecondaryView->SetWeight(weight);

	m_pPrimaryView->UpdateView();
	m_pSecondaryView->UpdateView();
}

void MainWindowBasic::on_comboBox_name_first_currentIndexChanged(const QString& fullname)
{
    TRACE(2, "MainWindowBasic::on_comboBox_name_first_currentIndexChanged(fullname=%s)", fullname.toUtf8().data());

    if (ControllerBasic* pControllerBasic = dynamic_cast<ControllerBasic*>(m_pController); pControllerBasic != nullptr)
        pControllerBasic->SetCurrentFightFighterName(FighterEnum::First, fullname);
    else
        Q_ASSERT("Could not cast Controller to ControllerBasic");
}

void MainWindowBasic::on_comboBox_name_second_currentIndexChanged(const QString& fullname)
{
    TRACE(2, "MainWindowBasic::on_comboBox_name_second_currentIndexChanged(fullname=%s)", fullname.toUtf8().data());

    if (ControllerBasic* pControllerBasic = dynamic_cast<ControllerBasic*>(m_pController); pControllerBasic != nullptr)
        pControllerBasic->SetCurrentFightFighterName(FighterEnum::Second, fullname);
    else
        Q_ASSERT("Could not cast Controller to ControllerBasic");
}

void MainWindowBasic::on_checkBox_golden_score_clicked(const bool checked)
{
    TRACE(2, "MainWindowBasic::on_checkBox_golden_score_clicked(checked=%d)", checked);
    const QString name = m_pUi->comboBox_fight_category->currentText();
	FightCategory t(name);
	m_pCategoryManager->GetCategory(name, t);

    m_pController->SetCurrentFightGoldenScore(checked);
	//> Set this before setting the time.
	//> Setting time will then update the views.

	if (checked)
	{
		if (m_pController->GetRules()->IsOption_OpenEndGoldenScore())
		{
            m_pController->SetRoundTime(QTime(0,0,0,0));
		}
		else
		{
            m_pController->SetRoundTime(QTime(0,0,0,0).addSecs(t.GetGoldenScoreTime()));
		}
	}
	else
	{
        m_pController->SetRoundTime(QTime(0,0,0,0).addSecs(t.GetRoundTime()));
	}
}

void MainWindowBasic::on_comboBox_fight_category_currentIndexChanged(const QString& s)
{
    TRACE(2, "MainWindowBasic::on_comboBox_fight_category_currentIndexChanged(s=%s)", s.toUtf8().data());
    FightCategory category(s);
	m_pCategoryManager->GetCategory(s, category);

	// add weights
	m_pUi->comboBox_weight->clear();
	m_pUi->comboBox_weight->addItems(category.GetWeightsList());

	// trigger round time update
	on_checkBox_golden_score_clicked(m_pUi->checkBox_golden_score->checkState());

	m_pController->OverrideRoundTimeOfFightMode(category.GetRoundTime());
	m_pController->DoAction(Ipponboard::eAction_ResetAll);

	m_pPrimaryView->SetCategory(s);
	m_pSecondaryView->SetCategory(s);
	m_pPrimaryView->UpdateView();
	m_pSecondaryView->UpdateView();
}

void MainWindowBasic::fill_suitable_fighters(const QString& weight, const FightCategory& category)
{
    TRACE(2, "MainWindowBasic::fill_suitable_fighters(weight=%s)", weight.toUtf8().data());

    // find suitable fighters
    m_CurrentFighterNames.clear();

	for (const Ipponboard::Fighter & f : m_fighterManager.m_fighters)
	{
        if ((f.GetWeight() == weight || f.GetWeight().isEmpty()) &&
            (f.GetCategory() == category.ToString() || f.GetCategory().isEmpty() ))
		{
            const QString fullName = QString("%1 %2").arg(f.GetFirstName(), f.GetLastName());

			m_CurrentFighterNames.push_back(fullName);
		}
	}

	m_CurrentFighterNames.sort();

	m_pUi->comboBox_name_first->clear();
	m_pUi->comboBox_name_first->addItems(m_CurrentFighterNames);
	m_pUi->comboBox_name_second->clear();
	m_pUi->comboBox_name_second->addItems(m_CurrentFighterNames);
}

void MainWindowBasic::update_fighters(const QString& fullname)
{
    TRACE(2, "MainWindowBasic::update_fighters(fullname=%s)", fullname.toUtf8().data());
    if (fullname.isEmpty())
		return;

    QString firstName = fullname;
	QString lastName;

    if (const int pos = fullname.indexOf(' '); pos < fullname.size())
	{
        firstName = fullname.left(pos);
        lastName = fullname.mid(pos + 1);
	}

	const QString weight = m_pUi->comboBox_weight->currentText();
    const QString category = m_pUi->comboBox_fight_category->currentText();

	Ipponboard::Fighter fNew(firstName, lastName);
    fNew.SetWeight(weight);
    fNew.SetCategory(category);

	m_fighterManager.AddFighter(fNew); // only adds fighter if new
}

void MainWindowBasic::update_statebar()
{
    TRACE(2, "MainWindowBasic::update_statebar()");
    MainWindowBase::update_statebar();

//    if (Gamepad::eState_ok != m_pGamepad->GetState())
//    {
//        m_pUi->label_controller_state->setText(tr("No controller detected!"));
//    }
//    else
//    {
//        QString controllerName = QString::fromWCharArray(m_pGamepad->GetProductName());
//        m_pUi->label_controller_state->setText(tr("Using controller %1").arg(controllerName));
//    }
	ui_check_rules_items();

//    if (m_pController->GetOption(eOption_Use2013Rules))
//    {
//        m_pUi->actionRulesClassic->setChecked(false);
//        m_pUi->actionRules2013->setChecked(true);
//        m_pUi->actionRules2017->setChecked(false);
//    }
//    else
//    {
//        m_pUi->actionRulesClassic->setChecked(true);
//        m_pUi->actionRules2013->setChecked(false);
//        m_pUi->actionRules2017->setChecked(false);
//    }
}

void MainWindowBasic::attach_primary_view()
{
    TRACE(2, "MainWindowBasic::attach_primary_view()");

    if(QWidget* pWidget = dynamic_cast<QWidget*>(m_pPrimaryView.get()))
        m_pUi->verticalLayout_3->insertWidget(0, pWidget, 0);
}

void MainWindowBasic::retranslate_Ui()
{
    TRACE(2, "MainWindowBasic::retranslate_Ui()");

    m_pUi->retranslateUi(this);
}

void MainWindowBasic::ui_check_language_items()
{
    TRACE(2, "MainWindowBasic::ui_check_language_items()");

    m_pUi->actionLang_Deutsch->setChecked("de" == m_Language);
	m_pUi->actionLang_English->setChecked("en" == m_Language);
	m_pUi->actionLang_Dutch->setChecked("nl" == m_Language);

	// don't forget second implementation!
}

void MainWindowBasic::ui_check_rules_items()
{
    TRACE(2, "MainWindowBasic::ui_check_rules_items()");

    const auto rules = m_pController->GetRules();
	m_pUi->actionRulesClassic->setChecked(rules->IsOfType<ClassicRules>());
	m_pUi->actionRules2013->setChecked(rules->IsOfType<Rules2013>());
	m_pUi->actionRules2017->setChecked(rules->IsOfType<Rules2017>());
	m_pUi->actionRules2017U15->setChecked(rules->IsOfType<Rules2017U15>());
	m_pUi->actionRules2018->setChecked(rules->IsOfType<Rules2018>());
    m_pUi->actionRules2025->setChecked(rules->IsOfType<Rules2025>());

	if (rules->IsOfType<ClassicRules>())
	{
		m_pUi->label_usedRules->setText(m_pUi->actionRulesClassic->text());
	}
	else if (rules->IsOfType<Rules2013>())
	{
		m_pUi->label_usedRules->setText(m_pUi->actionRules2013->text());
	}
	else if (rules->IsOfType<Rules2017>())
	{
		m_pUi->label_usedRules->setText(m_pUi->actionRules2017->text());
	}
	else if (rules->IsOfType<Rules2017U15>())
	{
		m_pUi->label_usedRules->setText(m_pUi->actionRules2017U15->text());
	}
	else if (rules->IsOfType<Rules2018>())
	{
		m_pUi->label_usedRules->setText(m_pUi->actionRules2018->text());
	}
    else if (rules->IsOfType<Rules2025>())
    {
        m_pUi->label_usedRules->setText(m_pUi->actionRules2025->text());
    }

	m_pPrimaryView->UpdateView();
	m_pSecondaryView->UpdateView();
}

void MainWindowBasic::ui_check_show_secondary_view(const bool checked) const
{
    TRACE(2, "MainWindowBasic::ui_check_show_secondary_view(checked=%d)", checked);

    m_pUi->actionShow_SecondaryView->setChecked(checked);
	m_pUi->toolButton_viewSecondaryScreen->setChecked(checked);
}

void MainWindowBasic::on_actionAutoAdjustPoints_toggled(const bool checked)
{
    TRACE(2, "MainWindowBasic::on_actionAutoAdjustPoints_toggled(checked=%d)", checked);
    MainWindowBase::on_actionAutoAdjustPoints_toggled(checked);

	ui_update_used_options();
}

void MainWindowBasic::ui_update_used_options()
{
    TRACE(2, "MainWindowBasic::ui_update_used_options()");
    QString text;

	if (m_pController->IsAutoAdjustPoints())
	{
		text += tr("Auto adjust points");
	}
	else
	{
		text = "-";
	}

	m_pUi->label_usedOptions->setText(text);

}

void MainWindowBasic::on_actionViewInfoBar_toggled(const bool checked) const
{
    TRACE(2, "MainWindowBasic::on_actionViewInfoBar_toggled(checked=%d)", checked);
    for (int i = 0; i < m_pUi->horizontalLayout_infoBar->count(); ++i)
	{
		if (const auto item = m_pUi->horizontalLayout_infoBar->itemAt(i)->widget())
		{
			if (checked)
			{
				item->show();
			}
			else
			{
				item->hide();
			}
		}
	}

	if (checked)
	{
		m_pUi->line_infoBar->show();
	}
	else
	{
		m_pUi->line_infoBar->hide();
	}
}

void MainWindowBasic::on_toolButton_viewSecondaryScreen_toggled()
{
    TRACE(2, "MainWindowBasic::on_toolButton_viewSecondaryScreen_toggled()");
    on_actionShow_SecondaryView_triggered();
}

void MainWindowBasic::write_specific_settings(QSettings& settings)
{
    TRACE(2, "MainWindowBasic::write_specific_settings()");
    settings.beginGroup(EditionNameShort());
	{
		settings.remove("");
		settings.setValue(str_tag_MatLabel, m_MatLabel);
		settings.setValue(str_tag_rules, m_pController->GetRules()->Name());
	}
	settings.endGroup();
}

void MainWindowBasic::read_specific_settings(QSettings& settings)
{
    TRACE(2, "MainWindowBasic::read_specific_settings()");
    settings.beginGroup(EditionNameShort());
	{
        m_MatLabel = settings.value(str_tag_MatLabel, "ESV Siershahn").toString(); // value is also in settings dialog!
		m_pPrimaryView->SetMat(m_MatLabel);
		m_pSecondaryView->SetMat(m_MatLabel);

		// rules
		const auto rules = RulesFactory::Create(settings.value(str_tag_rules).toString());
		m_pController->SetRules(rules);
	}
	settings.endGroup();
}

void MainWindowBasic::UpdateGoldenScoreView()
{
    TRACE(2, "MainWindowBasic::UpdateGoldenScoreView()");
    m_pUi->checkBox_golden_score->setEnabled(m_pController->GetRules()->IsOption_OpenEndGoldenScore());
	m_pUi->checkBox_golden_score->setChecked(m_pController->IsGoldenScore());
}

void MainWindowBasic::load_fighters()
{
    TRACE(2, "MainWindowBasic::load_fighters()");

    const QString csvFile(fm::GetSettingsFilePath(GetFighterFileName().toLatin1()));

    if (!QFile::exists(csvFile))
	{
		// silently ignore
		return;
	}

	if (QString errorMsg; !m_fighterManager.ImportFighters(csvFile, FighterManager::DefaultExportFormat(), errorMsg))
	{
		QMessageBox::critical(
			this,
			QCoreApplication::applicationName(),
			errorMsg);
	}
}

void MainWindowBasic::save_fighters()
{
    TRACE(2, "MainWindowBasic::save_fighters()");

    const QString csvFile(fm::GetSettingsFilePath(GetFighterFileName().toLatin1()));

    if (QString errorMsg; !m_fighterManager.ExportFighters(csvFile, FighterManager::DefaultExportFormat(), errorMsg))
	{
		QMessageBox::critical(
			this,
			QCoreApplication::applicationName(),
			errorMsg);
	}
}

void MainWindowBasic::closeEvent(QCloseEvent* event)
{
    TRACE(2, "MainWindowBasic::closeEvent()");
	save_fighters();

    MainWindowBase::closeEvent(event);
}

