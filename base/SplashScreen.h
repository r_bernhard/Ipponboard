// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef WIDGETS__SPLASHSCREEN_H_
#define WIDGETS__SPLASHSCREEN_H_

#include <QDialog>
#include <QDate>

// forwards
namespace Ui { class SplashScreen; }

namespace Ipponboard
{
class SplashScreen : public QDialog
{
	Q_OBJECT

public:
	struct Data
	{
		QDate date;
		QString text;
		QString info;
	};

	SplashScreen(Data const& data, QWidget* parent = 0);
	~SplashScreen();

	void SetImageStyleSheet(QString const& text) const;

protected:
	void changeEvent(QEvent* e);

private:
	Ui::SplashScreen* ui;

private slots:
	void on_commandLinkButton_startSingleVersion_pressed();
	void on_commandLinkButton_startTeamVersion_pressed();
    void on_commandLinkButton_startEncounterVersion_pressed();
    void on_commandLinkButton_donate_pressed();
};
} // namespace Ipponboard
#endif  // WIDGETS__SPLASHSCREEN_H_
