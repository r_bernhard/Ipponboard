# Copyright 2018 Florian Muecke. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE.txt file.
#------------------------------------------------------------------------------
QT += xmlpatterns

# This app depends on:
#   - core
# These need to be build first.
#PRE_TARGETDEPS += ../core

TEMPLATE = app
LANGUAGE = C++
CONFIG += windows precompile_header
DEFINES += _WIN32

# Use Precompiled headers (PCH)
# (inclusion of header in HEADERS section is not required!)
#PRECOMPILED_HEADER  = ../base/pch.h
#disabled for mingw!

INCLUDEPATH += $$quote($$(BOOST_DIR))

QMAKE_LIBDIR += $$quote($$(BOOST_DIR)/stage/lib) \
    ../lib

CONFIG(debug, release|debug) {
	prebuildhook.target = Makefile.Debug
    TARGET = Ipponboard_d
    QMAKE_LIBS += -lshell32
	DESTDIR = ../_build/bin/Debug
}

CONFIG(release, release|debug) {
    prebuildhook.target = Makefile.Release
	TARGET = Ipponboard
    QMAKE_LIBS += -lshell32
	DESTDIR = ../_build/bin/Release
}

QMAKE_LFLAGS += /SUBSYSTEM:WINDOWS,5.01

# Auto select compiler 
win32-g++: COMPILER = mingw
win32-msvc2013: COMPILER = msvc
win32-msvc2015: COMPILER = msvc
win32-msvc2017: COMPILER = msvc

contains(COMPILER, mingw) {
	QMAKE_CXXFLAGS += -std=c++17
	# get rid of some nasty boost warnings
    QMAKE_CXXFLAGS += -Wno-unused-local-typedef
}

contains(COMPILER, msvc) {
    QMAKE_CXX += /FS /MP /std:c++17
    DEFINES += "WINVER=0x0501"
    DEFINES += WIN32 _WIN32_WINNT=0x0501 _WITH_GAMEPAD_ __QT4__

    # remove unneccessary output files
    #QMAKE_POST_LINK += del /Q ../_build/bin/$${TARGET}.exp ../_build/bin/$${TARGET}.lib>nul 
}

CONFIG(debug, release|debug) {
	# copy all needed files to destdir
    QMAKE_POST_LINK += call _copy_files_qt4.cmd
}

CONFIG(release, release|debug) {
	# copy all needed files to destdir
    QMAKE_POST_LINK += call _copy_files_qt4.cmd -release
}

HEADERS = pch.h \
    edition_basic/MainWindowBasic.h \
	edition_team/MainWindowTeam.h \
	edition_encounter/MainWindowEncounter.h \
    edition_basic/AddFighterDlg.h \
    ClubManager.h \
    ClubManagerDlg.h \
    DonationManager.h \
    MainWindowBase.h \
	edition_team/ModeManagerDlg.h \
    SettingsDlg.h \
	edition_team/ScoreScreen.h \
    SplashScreen.h \
    View.h \
    edition_basic/FightCategoryManagerDlg.h \
    edition_basic/FightCategoryManager.h \
    edition_basic/FighterManager.h \
    edition_basic/FighterManagerDlg.h \
    ../util/array_helpers.h \
    ../util/path_helpers.h \
    ../widgets/ScaledImage.h \
    ../widgets/ScaledText.h \
    ../util/SimpleCsvFile.hpp \
	../core/Controller.h \
	../core/edition_basic/ControllerBasic.h \
	../core/edition_team/ControllerTeam.h \
	../core/edition_encounter/ControllerEncounter.h \
	../core/Enums.h \
	../core/Fight.h \
	../core/edition_basic/FightCategory.h \
	../core/iController.h \
	../core/iControllerCore.h \
	../core/Score.h \
	../core/StateMachine.h \
	../core/Tournament.h \
	../core/Fighter.h \
	../core/TournamentMode.h \
	../core/edition_team/TableModelTeam.h \
	../core/edition_encounter/TableModelEncounter.h \
	../core/GamepadConfig.h \
    ../core/Rules.h \
    UpdateChecker.h \
    VersionComparer.h \
    ../core/iGoldenScoreView.h \
    ../core/iView.h \
    ../util/json/json.hpp \
    edition_basic/FightCategoryParser.h \
    ClubParser.h


SOURCES = Main.cpp \
    edition_basic/MainWindowBasic.cpp \
    edition_team/MainWindowTeam.cpp \
    edition_encounter/MainWindowEncounter.cpp \
    ../util/json/json.cpp \
	edition_basic/AddFighterDlg.cpp \
    ClubManager.cpp \
    ClubManagerDlg.cpp \
    DonationManager.cpp \
    SettingsDlg.cpp \
    View.cpp \
    edition_basic/FightCategoryManagerDlg.cpp \
    edition_basic/FightCategoryManager.cpp \
    MainWindowBase.cpp \
	edition_team/ModeManagerDlg.cpp \
    edition_basic/FighterManager.cpp \
    edition_basic/FighterManagerDlg.cpp \
	edition_team/ScoreScreen.cpp \
    SplashScreen.cpp \
    ../widgets/ScaledImage.cpp \
    ../widgets/ScaledText.cpp \
	../core/Controller.cpp \
	../core/edition_basic/ControllerBasic.cpp \
	../core/edition_team/ControllerTeam.cpp \
	../core/edition_encounter/ControllerEncounter.cpp \
	../core/edition_basic/FightCategory.cpp \
	../core/Fighter.cpp \
	../core/Fight.cpp \
	../core/Score.cpp \
	../core/StateMachine.cpp \
	../core/TournamentMode.cpp \
	../core/edition_team/TableModelTeam.cpp \
	../core/edition_encounter/TableModelEncounter.cpp \
    ../core/Rules.cpp \
    UpdateChecker.cpp \
    VersionComparer.cpp \
    edition_basic/FightCategoryParser.cpp

FORMS = edition_basic/MainWindowBasic.ui \
    edition_team/MainWindowTeam.ui \
    edition_encounter/MainWindowEncounter.ui \
    view_vertical_single.ui \
    edition_basic/AddFighterDlg.ui \
    ClubManagerDlg.ui \
	edition_team/ScoreScreen.ui \
    SettingsDlg.ui \
	edition_team/ModeManagerDlg.ui \
    edition_basic/FightCategoryManagerDlg.ui \
    edition_basic/FighterManagerDlg.ui \
    view_horizontal.ui \
    SplashScreen.ui \

OTHER_FILES += \
    TournamentModes.ini

RESOURCES += ipponboard.qrc
TRANSLATIONS = ../i18n/de.ts \
    ../i18n/nl.ts

win32:RC_FILE = ipponboard.rc
