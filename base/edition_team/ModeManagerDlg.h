// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef MODEMANAGERDLG_H
#define MODEMANAGERDLG_H

#include "../core/TournamentMode.h"
#include "../util/DialogResult.h"
#include <QDialog>
#include <memory>

// forwards
class QStringList;
namespace Ui { class ModeManagerDlg; }

namespace Ipponboard
{
class ModeManagerDlg : public QDialog, public fm::DialogResult<TournamentMode::List>
{
	Q_OBJECT

public:
	explicit ModeManagerDlg(
        TournamentMode::List const& modes,
		QStringList const& templates,
		QString const& currentModeId,
		QWidget* parent = 0);
	~ModeManagerDlg();

private slots:
	// comboBoxes
	void on_comboBox_mode_currentIndexChanged(int i);
	void on_comboBox_template_currentIndexChanged(QString const& s);
	void on_comboBox_rules_currentIndexChanged(int);
	// checkBoxes
	void on_checkBox_timeOverrides_toggled(bool checked);
	void on_checkBox_doubleWeights_toggled(bool checked);
	void on_checkBox_allSubscoresCount_toggled(bool checked);
	// buttons
	void on_toolButton_add_clicked();
	void on_toolButton_remove_clicked();
	// spin controls
	void on_spinBox_rounds_valueChanged(int i);
	void on_spinBox_fightTimeSeconds_valueChanged(int i);
	void on_spinBox_fightTimeMinutes_valueChanged(int i);
	// line edits
	void on_lineEdit_weights_textChanged(QString const& s);
	void on_lineEdit_title_textChanged(QString const& s);
	void on_lineEdit_subtitle_textChanged(QString const& s);
	void on_lineEdit_timeOverrides_textChanged(QString const& s);

private:
    void update_fights_per_round(TournamentMode const& mode) const;
    bool has_Mode() const { return m_currentIndex != -1;  }

    TournamentMode& GetMode(int i);
    TournamentMode& GetCurrentMode() { return GetMode(m_currentIndex); }
    TournamentMode m_DefaultMode{};
    std::shared_ptr<Ui::ModeManagerDlg> m_pUi;  //TODO: use unique_ptr
	int m_currentIndex;
};
} // namespace Ipponboard

#endif // MODEMANAGERDLG_H
