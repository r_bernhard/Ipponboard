#include "../util/debug.h"
#include "Fight.h"

using namespace Ipponboard;

///////////////////////////////////////////////////////////////////////////////
// Constructor / Destructor
///////////////////////////////////////////////////////////////////////////////
Fight::Fight(Score const& first, Score const& second)
    : m_scores { first, second }
    , m_fighters { Fighter(QString(), QString()), Fighter(QString(), QString())} // empty fighter
    , m_weight_class()
    , m_rules(new ClassicRules)
    , m_is_saved(false)
    , m_seconds_elapsed(0)
    , m_round_time_seconds(0)
    , m_isGoldenScore(false)
{
    TRACE(2, "Fight::Fight()");

    m_rules->SetCountSubscores(false);
}

Fight::~Fight()
{
    TRACE(2, "Fight::~Fight()");
}

///////////////////////////////////////////////////////////////////////////////
// Other
///////////////////////////////////////////////////////////////////////////////

int Fight::GetGoldenScoreTime() const
{
    TRACE(2, "Fight::GetGoldenScoreTime()");
    if (IsGoldenScore() && m_seconds_elapsed < 0)
        return -m_seconds_elapsed;

    return 0;
}

int Fight::GetScorePoints(const FighterEnum who) const
{
    TRACE(6, "Fight::GetScorePoints()");
    const FighterEnum other = GetUkeFromTori(who);

    if (HasWon(who))
    {
        if (GetScore(who).Ippon() || m_rules->IsAwaseteIppon(GetScore(who)))
        {
            return eScore_Ippon;
        }

        // Only the fight deciding point is taken into account!
        if (GetScore(who).Wazaari() > 0 && GetScore(who).Wazaari() > GetScore(other).Wazaari())
        {
            return eScore_Wazaari;
        }

        if (GetScore(who).Yuko() > GetScore(other).Yuko())
        {
            return eScore_Yuko;
        }

        if ((!m_rules->IsOption_ShidoAddsPoint() || IsGoldenScore()) && GetScore(who).Shido() < GetScore(other).Shido())
        {
            return eScore_Shido;
        }

        //TODO: Hantei!
    }
    else
    {
        if (!HasWon(other))
        {
            return eScore_Hikewake;
        }
        else if (m_rules->IsOption_CountSubscores())
        {
            // Special rule for Jugendliga
            if (GetScore(who).Wazaari() > GetScore(other).Wazaari())
            {
                return eScore_Wazaari;
            }
            else if (GetScore(who).Yuko() > GetScore(other).Yuko())
            {
                return eScore_Yuko;
            }
        }
    }

    return eScore_Lost;
}

bool Fight::HasWon(const FighterEnum who) const
{
    TRACE(6, "Fight::HasWon()");
    //unused: const FighterEnum other = GetUkeFromTori(who);

    if (const auto result = m_rules->CompareScore(*this); (who == FighterEnum::First && result < 0) || (who == FighterEnum::Second && result > 0))
    {
        return true;
    }

    return false;
}

int Fight::GetRemainingTime() const
{
    TRACE(2, "Fight::GetRemainingTime()");
    if (IsGoldenScore())
    {
        return 0;
    }

    return m_round_time_seconds - m_seconds_elapsed;
}

QString Fight::GetTimeRemainingString() const
{
    TRACE(2, "Fight::GetTimeRemainingString()");
    // get time display
    const auto isGoldenScore = m_seconds_elapsed < 0;

    const auto time_remaining = isGoldenScore ? GetGoldenScoreTime() : GetRemainingTime();
    const auto minutes = time_remaining / 60;
    const auto seconds = time_remaining % 60;

    return QString("%1%2:%3%4").arg(
        isGoldenScore ? "-" : "",
        QString::number(minutes),
        seconds < 10 ? "0" : "",
        QString::number(seconds));
}

QString Fight::GetTotalTimeElapsedString() const
{
    TRACE(2, "Fight::GetTotalTimeElapsedString()");
    // get time display
    const auto elapsed = IsGoldenScore() ? -m_seconds_elapsed + m_round_time_seconds : m_seconds_elapsed;

    const int minutes = elapsed / 60;
    const int seconds = elapsed % 60;

    return QString("%1:%3%4").arg(
        QString::number(minutes),
        seconds < 10 ? "0" : "",
        QString::number(seconds));
}

bool Fight::SetElapsedFromTotalTime(QString s)
{
    TRACE(2, "Fight::SetElapsedFromTotalTime(s=%s)", s.toUtf8().data());
    int secs = 0;

    if (const auto parts = s.split(":"); parts.size() > 1)
    {
        secs += parts.at(0).toUInt() * 60;
        secs += parts.at(1).toUInt();
    }
    else
    {
        return false;
    }

    if (secs > m_round_time_seconds)
    {
        secs = m_round_time_seconds - secs;
        SetGoldenScore(true);
    }
    else
    {
        SetGoldenScore(false);
    }

    SetSecondsElapsed(secs);

    return true;
}



