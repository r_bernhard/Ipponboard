// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef FIGHTER_H
#define FIGHTER_H

#include <QString>

namespace Ipponboard
{
class Fighter
{
public:
    Fighter(QString const& firstName,
			QString const& lastName);

	inline bool operator==(Fighter const& other) const
	{
		return this == &other ||
               (m_first_name == other.m_first_name &&
                m_last_name == other.m_last_name &&
                m_club == other.m_club &&
                m_weight == other.m_weight &&
                m_category == other.m_category
                );
	}

	inline bool operator<(Fighter const& other) const
	{
        if (m_first_name < other.m_first_name)
			return true;

        if (other.m_first_name < m_first_name)
			return false;

        if (m_last_name < other.m_last_name)
			return true;

        if (other.m_last_name < m_last_name)
			return false;

        if (m_club < other.m_club)
			return true;

        if (other.m_club < m_club)
			return false;

        if (m_weight < other.m_weight)
			return true;

        if (other.m_weight < m_weight)
			return false;

        if (m_category < other.m_category)
            return true;

        if (other.m_category < m_category)
            return false;

		// equal
		return false;
	}

    // Properties
    QString GetName() const { return m_name; }
    void SetName(QString name) { m_name = name; }

    QString GetFirstName() const { return m_first_name; }
    QString GetLastName() const { return m_last_name; }

    QString GetClub() const { return m_club; }
    void SetClub(QString club) { m_club = club; }

    QString GetWeight() const { return m_weight; }
    void SetWeight(QString weight) { m_weight = weight; }

    QString GetCategory() const { return m_category; }
    void SetCategory(QString category) { m_category = category; }

private:
    QString m_name;
    QString m_first_name;
    QString m_last_name;
    QString m_club;
    QString m_weight;
    QString m_category;
    //QString m_nation;
};
}  // namespace Ipponboard

#endif // FIGHTER_H
