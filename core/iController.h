// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef BASE__ICONTROLLER_H_
#define BASE__ICONTROLLER_H_

#include "Enums.h"
#include "Score.h"
#include "Rules.h"

#include <QString>
#include <QTime>

namespace Ipponboard
{
class IView;
class IGoldenScoreView;
class AbstractRules;
class IControllerTeam;

class IController
{
public:
    virtual IController const* GetIController() const = 0;
    virtual IController* GetIController() = 0;

    //IControllerViews
        // IControllerView
    virtual int GetScore(FighterEnum whos, Score::Point point) const =0;
    virtual FighterEnum GetLastHolder() const = 0;
    virtual QString GetCurrentFightFighterLastName(FighterEnum) const = 0;
    virtual QString GetCurrentFightFighterFirstName(FighterEnum) const = 0;
    virtual QString const GetWeightClass() const = 0;

        // IControllerMainWindowBase
    virtual void RegisterView(IView* pView) = 0;
    virtual void RegisterView(IGoldenScoreView* pView) = 0;
    virtual EState GetCurrentState() const	= 0;
    virtual FighterEnum GetLead() const = 0;
    virtual QString GetTimeText(ETimer timer) const = 0;
    virtual void SetTimerValue(ETimer timer, const QString& value) = 0;
    virtual void SetAutoAdjustPoints(bool isActive) = 0;
    virtual void OverrideRoundTimeOfFightMode(int fightTimeSecs) = 0;
    virtual QString const& GetGongFile() const = 0;
    virtual void SetGongFile(const QString& gong_file) = 0;
    virtual void Gong() const = 0;

        // IControllerAllMainWindows
    virtual void DoAction(EAction action, FighterEnum who = FighterEnum::First, bool doRevoke = false) = 0;
    virtual void SetRoundTime(const QString& value) = 0;
    virtual void SetRoundTime(const QTime& time) = 0;
    //FIXME: virtual int GetRound() const = 0;
    virtual void SetCurrentFightGoldenScore(bool isGS) = 0;
    virtual bool IsGoldenScore() const = 0;
    virtual std::shared_ptr<AbstractRules> GetRules() const = 0;
    virtual void SetRules(std::shared_ptr<AbstractRules> rules) = 0;
    virtual bool IsAutoAdjustPoints() const = 0;
    virtual QString GetHomeLabel() const = 0;
    virtual QString GetGuestLabel() const = 0;
    virtual void SetLabels(QString const& home, QString const& guest) = 0;
    virtual void ClearFightsAndResetTimers() = 0;
};
} // namespace Ipponboard
#endif  // BASE__ICONTROLLER_H_
