#ifndef FIGHT_H
#define FIGHT_H

#include "Score.h"
#include "Enums.h"
#include "Fighter.h"
#include "Rules.h"

namespace Ipponboard
{
class Fight
{
public:
    // constructors / destructors
    Fight(Score const& first, Score const& second);
    virtual ~Fight();

    // properties/predicates
    Fighter const& GetFighter(FighterEnum fighterPos) const { return m_fighters[static_cast<int>(fighterPos)]; }
    Fighter& GetFighter(FighterEnum fighterPos) { return m_fighters[static_cast<int>(fighterPos)]; }
    void SetFighter(FighterEnum fighterPos, Fighter fighter) { m_fighters[fighterPos] = fighter; }
    void SetFighterName(FighterEnum fighterPos, QString name) { m_fighters[fighterPos].SetName(name); }

    QString GetWeightClass() const { return m_weight_class; }
    void SetWeightClass(QString weight_class) { m_weight_class = weight_class; }

    Score& GetScore(FighterEnum fighterPos) { return m_scores[static_cast<int>(fighterPos)]; }
    Score const& GetScore(FighterEnum fighterPos) const { return m_scores[static_cast<int>(fighterPos)]; }

    bool IsGoldenScore() const { return m_isGoldenScore; }
    void SetGoldenScore(bool val) { m_isGoldenScore = val; }

    int GetRoundSeconds() const { return m_round_time_seconds; }
    void SetRoundTime(int secs) { m_round_time_seconds = secs; }

    int GetSecondsElapsed() const { return m_seconds_elapsed; }
    void SetSecondsElapsed(int s) { m_seconds_elapsed = s; }

    bool IsSaved() const { return m_is_saved; }
    void SetIsSaved(bool is_saved) { m_is_saved = is_saved; }

    std::shared_ptr<AbstractRules> GetRules() const { return m_rules; }
    void SetRules(std::shared_ptr<AbstractRules> rules) { m_rules = rules; }

    // other
    int GetGoldenScoreTime() const;
    int GetScorePoints(FighterEnum who) const;
    bool HasWon(FighterEnum who) const;
    int GetRemainingTime() const;
    QString GetTimeRemainingString() const;
    bool SetElapsedFromTotalTime(QString s);
    QString GetTotalTimeElapsedString() const;

    // member variables
    std::shared_ptr<AbstractRules> m_rules; // TODO: this should be removed if possible

protected:
    enum
    {
        eScore_Ippon = 10,
        eScore_Wazaari = 7,
        eScore_Yuko = 5,
        eScore_Hantai = 1,
        eScore_Shido = 1,
        eScore_Hikewake = 0,
        eScore_Lost = 0
    };

private:
    // member variables
    Fighter m_fighters[2];
    Score m_scores[2];
    bool m_isGoldenScore;
    QString m_weight_class;
    bool m_is_saved;
    int m_seconds_elapsed;
    int m_round_time_seconds;
};
} // namespace Ipponboard

#endif // FIGHT_H
