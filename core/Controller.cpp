﻿// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#include "../util/debug.h"
#include "Controller.h"
#include "iView.h"
#include "iGoldenScoreView.h"
#include "Score.h"
#include "Enums.h"
#include "TournamentMode.h"
#include "StateMachine.h"
#include "Rules.h"

#include <QTimer>
#include <QFileInfo>
#include <QMessageBox>

#ifdef _WIN32
#include <QSound>
#ifndef __QT4__
//#include <QAudio>
#include <QAudioDeviceInfo>
#endif
#endif
#ifdef __linux__
#ifdef __QT4__
#include <QProcess>
#else
#include <QSound>
#endif
#endif

using namespace Ipponboard;

const char* const Controller::msg_Ippon = "Ippon";
const char* const Controller::msg_WazariAwaseteIppon = "Wazaari awasete ippon";
const char* const Controller::msg_Wazaari = "Wazaari";
const char* const Controller::msg_Yuko = "Yuko";
const char* const Controller::msg_Shido = "Shido";
const char* const Controller::msg_Hansokumake = "Hansokumake";
const char* const Controller::msg_Osaekomi = "Osae Komi";
const char* const Controller::msg_SonoMama = "Sono Mama";
//const char* const Controller::msg_Hantei = "Hantei";
const char* const Controller::msg_Hikiwaki = "Hikiwaki";
const char* const Controller::msg_Winner = "Winner";
const char* const emptyName = "--";

//=========================================================
Controller::Controller()
	: m_mode()
	, m_Tournament()
	, m_currentRound(0)
	, m_currentFight(0)
	, m_pSM(nullptr)
	, m_State(eState_TimerStopped)
	, m_pTimerMain(nullptr)
	, m_pTimerHold(nullptr)
	, m_pTimeMain(nullptr)
	, m_pTimeHold(nullptr)
    , m_Tori(FighterEnum::Nobody)
	, m_isSonoMama(false)
	, m_roundTime(0, 0, 0, 0)
	, m_options(0)
	, m_labelHome("HOME")
	, m_labelGuest("GUEST")
    ,m_isAutoAdjustPoints(true)
//=========================================================
{
    TRACE(2, "Controller::Controller()");

    m_pSM = new IpponboardSM();
    m_pSM->SetCore(this);
    m_pTimerMain = new QTimer(this);
	m_pTimerHold = new QTimer(this);
	m_pTimeMain = new QTime();
	m_pTimeHold = new QTime();

/*	InitTournament(m_mode);

	reset();
	m_pSM->start();

	connect(m_pTimerMain, SIGNAL(timeout()), this, SLOT(update_main_time()));
    connect(m_pTimerHold, SIGNAL(timeout()), this, SLOT(update_hold_time())); */
}

//=========================================================
Controller::~Controller()
//=========================================================
{
    TRACE(2, "Controller::~Controller()");
    m_views.clear();
	m_goldenScoreViews.clear();

    if (m_pTimeHold)
        delete m_pTimeHold;

    if (m_pTimeMain)
        delete m_pTimeMain;

    if (m_pSM)
        delete m_pSM;
}

//=========================================================
void Controller::Init()
//=========================================================
{
    TRACE(2, "Controller::Init()");

    InitTournament(m_mode);

    reset();
    m_pSM->start();

    connect(m_pTimerMain, SIGNAL(timeout()), this, SLOT(update_main_time()));
    connect(m_pTimerHold, SIGNAL(timeout()), this, SLOT(update_hold_time()));
}

//=========================================================
void Controller::InitTournament(TournamentMode const& mode)
//=========================================================
{
    TRACE(2, "Controller::InitTournament()");

    // TODO: Diesen Code anpassen an ein einheitliches TournamentModel.
    // Die Spezialitäten befinden sich dann in der abgeleiteten Klasse

    //m_TeamTournamentModels.clear();
	m_Tournament.clear();

	m_mode = mode;
	m_rules = RulesFactory::Create(m_mode.rules);
	m_rules->SetCountSubscores(m_mode.IsOptionSet(TournamentMode::str_Option_AllSubscoresCount));

	QStringList actualWeights = m_mode.weights.split(';');

	for (int round = 0; round < m_mode.nRounds; ++round)
	{
		PTournamentRound pRound(new TournamentRound());

		for (int fightNo = 0; fightNo < m_mode.FightsPerRound(); ++fightNo)
		{
			QString weight = m_mode.weightsAreDoubled ?
							 actualWeights[fightNo / 2] :
							 actualWeights[fightNo];

            Fight fight { Score(), Score() };
            fight.SetWeightClass(weight);
			fight.SetRoundTime(m_mode.GetFightDuration(weight));
            fight.SetRules(m_rules);
            fight.GetRules()->SetCountSubscores(m_mode.IsOptionSet(TournamentMode::str_Option_AllSubscoresCount));

            const Fighter emptyFighter = Fighter(QString(), QString());
            fight.SetFighter(FighterEnum::First, emptyFighter);
            fight.SetFighter(FighterEnum::Second, emptyFighter);

			pRound->emplace_back(fight);
		}

		m_Tournament.push_back(pRound);

        const PTableModelTeam pModel(new TableModelTeam(pRound));
        pModel->SetNumRows(m_mode.FightsPerRound());

        //m_TeamTournamentModels.push_back(pModel);
	}

	// set options AFTER configuring fights

	m_currentRound = 0;
	m_currentFight = 0;

	// set time and update views
    SetRoundTime(QTime(0,0,0,0).addSecs(m_mode.GetFightDuration(get_current_fight().GetWeightClass())));
}

//=========================================================
void Controller::DoAction(const EAction action, const FighterEnum whos, const bool doRevoke)
//=========================================================
{
    TRACE(2, "Controller::DoAction()");
    if (doRevoke)
	{
		switch (action)
		{
		case eAction_Yuko:
			m_pSM->process_event(IpponboardSM_::RevokeYuko(whos));
			break;

		case eAction_Wazaari:
			m_pSM->process_event(IpponboardSM_::RevokeWazaari(whos));
			break;

		case eAction_Ippon:
			m_pSM->process_event(IpponboardSM_::RevokeIppon(whos));
			break;

		case eAction_Shido:
		case eAction_Hansokumake:
			m_pSM->process_event(IpponboardSM_::RevokeShidoHM(whos));
			break;

		case eAction_ResetOsaeKomi:
			reset_timer_value(eTimer_Hold);
			break;

		case eAction_ResetMainTimer:
			reset_timer_value(eTimer_Main);
			break;

		default:
			return;
		}
	}
	else
	{
		switch (action)
		{
		case eAction_Hajime_Mate:
			if (eState_Holding == m_State)
				m_isSonoMama = true;
			else
				m_isSonoMama = false;

			m_pSM->process_event(IpponboardSM_::Hajime_Mate());
			break;

		case eAction_OsaeKomi_Toketa:
			m_pSM->process_event(IpponboardSM_::Osaekomi_Toketa());
			m_Tori = whos;
			m_isSonoMama = false;
			break;

		case eAction_Yuko:
			m_pSM->process_event(IpponboardSM_::Yuko(whos));
			break;

		case eAction_Wazaari:
			m_pSM->process_event(IpponboardSM_::Wazaari(whos));
			break;

		case eAction_Ippon:
			m_pSM->process_event(IpponboardSM_::Ippon(whos));
			break;

		case eAction_Shido:
			m_pSM->process_event(IpponboardSM_::Shido(whos));
			break;

		case eAction_Hansokumake:
			m_pSM->process_event(IpponboardSM_::Hansokumake(whos));
			break;

		case eAction_SetOsaekomi:
			m_Tori = whos;
			break;

		case eAction_ResetAll:
			m_pSM->process_event(IpponboardSM_::Reset());
			break;

		default:
			//Q_ASSERT(!"wrong action/action not handled!");
			break;
		}
	}

	//	if( eState_SonoMama == m_State && action != eAction_SonoMama_Yoshi )
	//		return;
	//

	//
	// handle golden score
	//
	// TODO: use state machine
	//
	// test cases:
	// (1) when golden score is active, wazaari or any other (except shido) will stop main timer
	// (2) main timer can be started, even if points are unequal (e.g. 1 shido) - no matter if GS or not
	// (3) main timer will not be stopped after the first achieved point in osaekomi in GS
	//
	// Do stop main timer if
	// - golden score is active
	// - we were not trying to start the timer itself
	// - if point <> shidos are changeed
	// - and the timer is not stopped
	//
    if (get_current_fight().IsGoldenScore()
			&& action != eAction_Hajime_Mate
			&& action != eAction_Shido
			&& eState_TimerStopped != EState(m_pSM->current_state()[0]))
	{
		// Note: In golden score the hold should not end after the first scored point!
		if (eState_Holding != EState(m_pSM->current_state()[0]))
		{
			if (const auto ruleSet = GetRules(); ruleSet->CompareScore(get_current_fight()) != 0)
			{
				m_pSM->process_event(IpponboardSM_::Hajime_Mate());
			}
		}
	}

	// set current state
	m_State = EState(m_pSM->current_state()[0]);

	update_views();
}

//=========================================================
FighterEnum Controller::GetLead() const
//=========================================================
{
    TRACE(2, "Controller::GetLead()");
    FighterEnum winner(FighterEnum::Nobody);

	switch (m_State)
	{
	case eState_TimerRunning:
	case eState_TimerStopped:
		{
			// determine who has the lead
            if (get_current_fight_score(FighterEnum::First).Wazaari() > get_current_fight_score(FighterEnum::Second).Wazaari())
			{
				winner = FighterEnum::First;
			}
            else if (get_current_fight_score(FighterEnum::First).Wazaari() < get_current_fight_score(FighterEnum::Second).Wazaari())
			{
				winner = FighterEnum::Second;
			}
			else  // GetScore_(eFirst).Wazaari() == GetScore_(eSecond).Wazaari()
			{
                if (get_current_fight_score(FighterEnum::First).Yuko() >
                        get_current_fight_score(FighterEnum::Second).Yuko())
				{
					winner = FighterEnum::First;
				}
                else if (get_current_fight_score(FighterEnum::First).Yuko() < get_current_fight_score(FighterEnum::Second).Yuko())
				{
					winner = FighterEnum::Second;
				}
				else  // GetScore_(eFirst).Yuko() == GetScore_(eSecond).Yuko()
				{
                    if (get_current_fight_score(FighterEnum::First).Shido() < get_current_fight_score(FighterEnum::Second).Shido() &&
                            get_current_fight_score(FighterEnum::Second).Shido() > 1)  // no "koka"!
					{
						winner = FighterEnum::First;
					}
                    else if (get_current_fight_score(FighterEnum::First).Shido() > get_current_fight_score(FighterEnum::Second).Shido() &&
                             get_current_fight_score(FighterEnum::First).Shido() > 1)
					{
						winner = FighterEnum::Second;
					}
					else
					{
						// equal ==> golden score in single tournament (Hantai is no more)
					}
				}
			}

			break;
		}

	case eState_SonoMama:
	case eState_Holding:
		winner = m_Tori;
		break;

	default:
		break;
	}

	return winner;
}

//=========================================================
void Controller::reset_timers()
{
    TRACE(2, "Controller::reset_timers()");
    m_State = eState_TimerStopped;

	Q_ASSERT(m_pTimeMain);
	Q_ASSERT(m_pTimeHold);

	m_pTimerMain->stop();
	reset_timer_value(eTimer_Main);

	m_pTimerHold->stop();
	reset_timer_value(eTimer_Hold);
    m_Tori = FighterEnum::Nobody;

	m_isSonoMama = false;
}

void Controller::reset()
//=========================================================
{
    TRACE(2, "Controller::reset()");
    ClearFightsAndResetTimers();
	SetFight(0, 0, "-XX", tr("First"), "", tr("Second"), "");

	update_views();
}

//=========================================================
void Controller::reset_timer_value(const Ipponboard::ETimer timer)
//=========================================================
{
    TRACE(4, "Controller::reset_timer_value()");
    // Note:
	//  just reset values - nothing more, nothing less

	if (eTimer_Main == timer)
	{
		*m_pTimeMain = m_roundTime;  // FIXME: set to 0 for golden score?
	}
	else if (eTimer_Hold == timer)
	{
        m_pTimeHold->setHMS(0, 0, 0, 0);

		if (!m_pTimerHold->isActive())
            m_Tori = FighterEnum::Nobody;
	}
}

//=========================================================
QString Controller::GetTimeText(const ETimer timer) const
//=========================================================
{
    TRACE(4, "Controller::GetTimeText()");
    QString ret;

	switch (timer)
	{
	case eTimer_Main:
		ret = m_pTimeMain->toString("m:ss");
		ret = ret.isEmpty() ? "0:00" : ret;
		break;

	case eTimer_Hold:
		ret = m_pTimeHold->toString("ss");
		ret = ret.isEmpty() ? "00" : ret;
		break;

	default:
		break;
	}

	return ret;
}

//=========================================================
int Controller::GetScore(const FighterEnum whos, const Score::Point point) const
//=========================================================
{
    TRACE(6, "Controller::GetScore()");
    int value(0);
    const Score& score = get_current_fight_score(whos);

    switch (point)
    {
    case Score::Point::Yuko:
        value = score.Yuko();
        break;

    case Score::Point::Wazaari:
        value = /*score.IsAwaseteIppon() ? 0 : */score.Wazaari();
        break;

    case Score::Point::Ippon:
        if (score.Ippon() || m_rules->IsAwaseteIppon(score))
            value = 1;
        else
            value = 0;

        break;

    case Score::Point::Shido:
        value = score.Shido();
        break;

    case Score::Point::Hansokumake:
        value = score.Hansokumake() ? 1 : 0;
        break;

    default:
        //Q_ASSERT(!"Unknown case in switch!");
        break;
    }

    return value;
}

//=========================================================
Fight& Controller::get_current_fight()
//=========================================================
{
    TRACE(2, "Controller::current_fight()");

    Fight& fight = m_Tournament.at(m_currentRound)->at(m_currentFight);
    return fight;
}

//=========================================================
Fight const& Controller::get_current_fight() const
//=========================================================
{
    TRACE(2, "Controller::current_fight()");

    Fight& fight = m_Tournament.at(m_currentRound)->at(m_currentFight);
    return fight;
}

//=========================================================
void Controller::SetCurrentFightFighterName(const Ipponboard::FighterEnum whos, const QString name)
//=========================================================
{
    TRACE(2, "Controller::SetFighterName(name=%s)", name.toUtf8().data());

    get_current_fight().GetFighter(whos).SetName(name);
    update_views();
}

//=========================================================
QString Controller::GetCurrentFightFighterName(const FighterEnum who) const
//=========================================================
{
    TRACE(2, "Controller::GetCurrentFightFighterName()");
    Q_ASSERT(who == FighterEnum::First || who == FighterEnum::Second);

    QString name = get_current_fight().GetFighter(who).GetName();

    // shorten name

    if (const int pos = name.indexOf(' '); pos != -1)
    {
        name = name.left(1) + ". " + name.mid(pos, name.length() - pos);
    }

    return name;
}

//=========================================================
QString Controller::GetCurrentFightFighterLastName(const Ipponboard::FighterEnum who) const
//=========================================================
{
    Q_ASSERT(who == FighterEnum::First || who == FighterEnum::Second);

    const Fighter fighter = get_current_fight().GetFighter(who);
    QString name = fighter.GetName();

	// get last name

    if (const int pos = name.indexOf(' '); pos != -1)
	{
		name = name.mid(pos + 1, name.length() - pos);
	}

    TRACE(2, "Controller::GetCurrentFightFighterLastName()=%s", name.toUtf8().data());

    return name;
}

//=========================================================
QString Controller::GetCurrentFightFighterFirstName(const Ipponboard::FighterEnum who) const
//=========================================================
{
    Q_ASSERT(who == FighterEnum::First || who == FighterEnum::Second);

    const Fighter fighter = get_current_fight().GetFighter(who);
    QString name = fighter.GetName();

	// get first name

    if (const int pos = name.indexOf(' '); pos != -1)
	{
		name = name.left(pos);
	}
	else
	{
		name = QString();
	}

    TRACE(2, "Controller::GetCurrentFightFighterFirstName()=%s", name.toUtf8().data());
    return name;
}

//=========================================================
QString Controller::GetCurrentFightFighterClub(const FighterEnum who) const
//=========================================================
{
    TRACE(2, "Controller::GetCurrentFightFighterClub()");
    Q_ASSERT(who == FighterEnum::First || who == FighterEnum::Second);

    return get_current_fight().GetFighter(who).GetClub();
}

//=========================================================
void Controller::SetTimerValue(const Ipponboard::ETimer timer, const QString& value)
//=========================================================
{
    TRACE(2, "Controller::SetTimerValue()");
    if (eState_TimerStopped == m_State ||
			eState_SonoMama == m_State)
	{
		if (eTimer_Main == timer)
		{
			// don't allow invalid times like "-1:22"
			if (const QTime newTime = QTime::fromString(value, "m:ss"); newTime.isValid())
			{
				*m_pTimeMain = newTime;
			}
		}
		else if (eTimer_Hold == timer)
		{
			m_pTimeHold->setHMS(0, 0, value.toInt());
		}

		update_views();
	}
}

//=========================================================
void Controller::SetRoundTime(const QString& value)
//=========================================================
{
    TRACE(2, "Controller::SetRoundTime()");
    SetRoundTime(QTime::fromString(value, "m:ss"));
}

//=========================================================
void Controller::SetRoundTime(QTime const& time)
//=========================================================
{
    TRACE(2, "Controller::SetRoundTime()");
    m_roundTime = time;
	*m_pTimeMain = time;

	update_views();
}

//=========================================================
void Controller::SetWeightClass(QString const& c)
//=========================================================
{
    TRACE(2, "Controller::SetWeightClass()");
    m_weight_class = c.isEmpty() ? emptyName : c;
}

//=========================================================
void Controller::SetCurrentFightGoldenScore(const bool isGS)
//=========================================================
{
    TRACE(2, "Controller::SetCurrentFightGoldenScore(isGS=%d)", isGS);

    get_current_fight().SetGoldenScore(isGS);

	if (isGS && GetRules()->IsOption_OpenEndGoldenScore())
	{
        *m_pTimeMain = QTime(0,0,0,0).addSecs(get_current_fight().GetGoldenScoreTime());
	}
	else
	{
        *m_pTimeMain = QTime(0,0,0,0).addSecs(get_current_fight().GetRemainingTime());
	}

	update_views();
}

void Controller::SetRules(const std::shared_ptr<AbstractRules> rules)
{
    TRACE(2, "Controller::SetRules()");
    m_rules = rules;

	for (auto const & pRound : m_Tournament)
	{
		for (auto & fight : *pRound)
		{
            fight.m_rules = rules;
		}
	}
}

//=========================================================
void Controller::SetOption(const EOption option, const bool isSet)
//=========================================================
{
    TRACE(2, "Controller::SetOption(option=%d, isSet=%d)", option, isSet);
    m_options.set(option, isSet);

	// TODO: remove maybe
	if (eOption_AllSubscoresCount == option)
	{
		m_rules->SetCountSubscores(isSet);
	}
}

//=========================================================
void Controller::SetLabels(const QString& home, const QString& guest)
//=========================================================
{
    TRACE(2, "Controller::SetLabels(home=%s, guest=%s)", home.toUtf8().data(), guest.toUtf8().data());
    m_labelHome = home;
	m_labelGuest = guest;
}

//=========================================================
void Controller::Gong() const
//=========================================================
{
    TRACE(2, "Controller::Gong()");
#ifdef _WIN32
#ifdef __QT4__
	if(QSound::isAvailable())
#else
	if (!QAudioDeviceInfo::availableDevices(QAudio::AudioOutput).isEmpty())
#endif
        QSound::play(m_gongFile);
    else
        QMessageBox::information(0, tr("Error"), tr("No sound device found"));
#endif
#ifdef __linux__
#ifdef __QT4__
    QProcess::startDetached("/usr/bin/aplay", QStringList() << m_gongFile);
#else
    QSound::play(m_gongFile);
#endif
#endif
}

//=========================================================
void Controller::RegisterView(IView* pView)
//=========================================================
{
    TRACE(2, "Controller::RegisterView()");
    m_views.insert(pView);

	// do not call UpdateViews here as views may not have been fully created
}

//=========================================================
void Controller::RegisterView(IGoldenScoreView* pView)
//=========================================================
{
    TRACE(2, "Controller::RegisterView()");
    m_goldenScoreViews.insert(pView);
}

//=========================================================
void Controller::start_timer(const ETimer t)
//=========================================================
{
    TRACE(2, "Controller::start_timer(ETimer=%d)", t);
    if (eTimer_Main == t)
	{
		m_pTimerMain->start(1000);
	}
	else
	{
		m_pTimerHold->start(1000);
	}
}

//=========================================================
void Controller::stop_timer(const ETimer t)
//=========================================================
{
    TRACE(2, "Controller::stop_timer(ETimer=%d)", t);
    m_pTimerHold->stop();

	if (eTimer_Main == t)
	{
		m_pTimerMain->stop();
	}
}

//=========================================================
void Controller::save_current_fight()
//=========================================================
{
    TRACE(2, "Controller::save_current_fight()");
    const auto elapsed = is_golden_score() ? m_pTimeMain->secsTo(QTime()) : m_pTimeMain->secsTo(m_roundTime);
    get_current_fight().SetSecondsElapsed(elapsed);
    get_current_fight().SetIsSaved(true);
}

//=========================================================
void Controller::reset_current_fight()
//=========================================================
{
    TRACE(2, "Controller::reset_current_fight()");
    m_pTimerHold->stop();
	m_pTimerMain->stop();
	m_pTimeHold->setHMS(0, 0, 0, 0);
    m_Tori = FighterEnum::Nobody;

	// just clear the score, not the names
    get_current_fight_score(FighterEnum::First) = Score();
    get_current_fight_score(FighterEnum::Second) = Score();

	//FIXME: make fight resetting more robust
    Fight& fight = get_current_fight();
	fight.SetSecondsElapsed(0);
	fight.SetGoldenScore(false);
    fight.SetIsSaved(false);
    fight.SetRules(m_rules);
    m_roundTime = QTime(0,0,0,0).addSecs(m_mode.GetFightDuration(get_current_fight().GetWeightClass()));
	*m_pTimeMain = m_roundTime;

	update_views();
}

//=========================================================
void Controller::reset_timer(const ETimer t)
//=========================================================
{
    TRACE(2, "Controller::reset_timer(t=%d)", t);
    // called by statemachine

	reset_timer_value(t);
	update_views();
}

//=========================================================
Score& Controller::get_current_fight_score(const FighterEnum who)
//=========================================================
{
    TRACE(2, "Controller::get_current_fight_score()");
    Q_ASSERT(who == FighterEnum::First || who == FighterEnum::Second);

    return get_current_fight().GetScore(who);
}

//=========================================================
Score const& Controller::get_current_fight_score(const FighterEnum who) const
//=========================================================
{
    TRACE(6, "Controller::get_current_fight_score()");
    Q_ASSERT(who == FighterEnum::First || who == FighterEnum::Second);

    return get_current_fight().GetScore(who);
}

//=========================================================
int Controller::get_time(const ETimer t) const
//=========================================================
{
    TRACE(2, "Controller::get_time(t=%d)", t);
    if (eTimer_Hold == t)
	{
        return -m_pTimeHold->secsTo(QTime(0,0,0,0));
	}
	else
	{
        return m_pTimeMain->secsTo(QTime(0,0,0,0));
	}
}
//=========================================================
void Controller::ClearFightsAndResetTimers()
//=========================================================
{
    TRACE(2, "Controller::ClearFightsAndResetTimers()");
    reset_timers();

	for (unsigned int round(0); round < m_Tournament.size(); ++round)
	{
		for (size_t fight(0); fight < m_Tournament[0]->size(); ++fight)
		{
			SetFight(round, fight, "", "", "", "", "");
			m_Tournament[round]->at(fight).SetSecondsElapsed(0);
		}
	}

	m_currentRound = 0;
	m_currentFight = 0;

	update_views();
}

//=========================================================
void Controller::SetFight(
	const unsigned int round_index, const size_t fight_index,
	const QString& weight,
	const QString& first_player_name, const QString& first_player_club,
	const QString& second_player_name, const QString& second_player_club,
	int yuko1, int wazaari1, const int ippon1, int shido1, const int hansokumake1,
	int yuko2, int wazaari2, const int ippon2, int shido2, const int hansokumake2) const
//=========================================================
{
    TRACE(2, "Controller::SetFight(weight=%s, first_player_name=%s, second_player_name=%s)", weight.toUtf8().data(), first_player_name.toUtf8().data(), second_player_name.toUtf8().data());

    Ipponboard::Fight fight = Fight(Score(), Score());
    fight.SetWeightClass(weight);
	fight.SetSecondsElapsed(0);
    fight.SetRules(m_rules);

	// TODO: set fight.max_time_in_seconds
	// TODO: set fight.allSubscoresCount
	// (not set as setting fights is currently not used...): 130512

    constexpr auto Yuko = Score::Score::Point::Yuko;
    constexpr auto Wazaari = Score::Score::Point::Wazaari;
    constexpr auto Ippon = Score::Score::Point::Ippon;
    constexpr auto Shido = Score::Score::Point::Shido;
    constexpr auto Hansokumake = Score::Score::Point::Hansokumake;

    fight.GetFighter(FighterEnum::First).SetName(first_player_name.isEmpty() ? emptyName : first_player_name);
    fight.GetFighter(FighterEnum::First).SetClub(first_player_club);
    fight.GetScore(FighterEnum::First).Clear();

	while (yuko1 != -1 && yuko1 > 0)
	{
        fight.GetScore(FighterEnum::First).Add(Yuko);
		--yuko1;
	}

	while (wazaari1 != -1 && wazaari1 > 0)
	{
        fight.GetScore(FighterEnum::First).Add(Wazaari);
		--wazaari1;
	}

	if (ippon1 > 0)
	{
        fight.GetScore(FighterEnum::First).Add(Ippon);
	}

	while (shido1 != -1 && shido1 > 0)
	{
        fight.GetScore(FighterEnum::First).Add(Shido);
		--shido1;
	}

	if (hansokumake1 > 0)
	{
        fight.GetScore(FighterEnum::First).Add(Hansokumake);
	}

    fight.GetFighter(FighterEnum::Second).SetName(second_player_name.isEmpty() ? emptyName : second_player_name);
    fight.GetFighter(FighterEnum::Second).SetClub(second_player_club);
    fight.GetScore(FighterEnum::Second).Clear();

	while (yuko2 != -1 && yuko2 > 0)
	{
        fight.GetScore(FighterEnum::Second).Add(Yuko);
		--yuko2;
	}

	while (wazaari2 != -1 && wazaari2 > 0)
	{
        fight.GetScore(FighterEnum::Second).Add(Wazaari);
		--wazaari2;
	}

	if (ippon2 > 0)
	{
        fight.GetScore(FighterEnum::Second).Add(Ippon);
	}

	while (shido2 != -1 && shido2 > 0)
	{
        fight.GetScore(FighterEnum::Second).Add(Shido);
		--shido2;
	}

	if (hansokumake2 > 0)
	{
        fight.GetScore(FighterEnum::First).Add(Hansokumake);
	}

	m_Tournament[round_index]->at(fight_index) = fight;

	update_views();
}

//=========================================================
void Controller::update_main_time()
//=========================================================
{
    TRACE(2, "Controller::update_main_time()");
    if (m_rules->IsOption_OpenEndGoldenScore() && is_golden_score())
	{
		*m_pTimeMain = m_pTimeMain->addSecs(1);
	}
	else
	{
		*m_pTimeMain = m_pTimeMain->addSecs(-1);

        const bool isTimeUp = QTime(0,0,0,0).secsTo(*m_pTimeMain) <= 0;

		// correct time again

		if (const int secsTo(QTime(0,0,0,0).secsTo(*m_pTimeMain)); secsTo < 0 || *m_pTimeMain > m_roundTime)
            m_pTimeMain->setHMS(0,0,0,0);

		if (eState_TimerRunning == m_State)
		{
			if (isTimeUp)
			{
				m_pSM->process_event(IpponboardSM_::TimeEndedEvent());
				m_State = EState(m_pSM->current_state()[0]);
				Gong();
			}
		}

		// else (stopped or ended) --> do nothing
	}

	update_views();
}

//=========================================================
void Controller::update_hold_time()
//=========================================================
{
    TRACE(2, "Controller::update_hold_time()");
    if (eState_Holding != m_State)
		return;

	*m_pTimeHold = m_pTimeHold->addSecs(1);

    if (const int secs = m_pTimeHold->second(); secs > 0 &&
			(m_rules->GetOsaekomiValue(Score::Score::Point::Yuko) == secs
			 || m_rules->GetOsaekomiValue(Score::Score::Point::Wazaari) == secs
			 || m_rules->GetOsaekomiValue(Score::Score::Point::Ippon) == secs))
	{
		m_pSM->process_event(IpponboardSM_::HoldTimeEvent(secs, m_Tori));
		m_State = EState(m_pSM->current_state()[0]);

		if (eState_TimerStopped == m_State)
			Gong();
	}

	update_views();
}

//=========================================================
void Controller::update_views() const
//=========================================================
{
    TRACE(2, "Controller::update_views()");
    for (auto const & pView : m_views)
	{
		pView->UpdateView();
	}

	for (auto const & pView : m_goldenScoreViews)
	{
		pView->UpdateGoldenScoreView();
	}
}
