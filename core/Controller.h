﻿// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef BASE__CONTROLLER_H_
#define BASE__CONTROLLER_H_

#include "Score.h"
#include "Tournament.h"
#include "iController.h"
#include "iControllerCore.h"
#include "TournamentMode.h"
#include "edition_team/TableModelTeam.h"
#include "StateMachine.h"
#include "../util/helpers.hpp"

#include <QObject>
#include <QTime>
#include <set>
#include <vector>
#include <bitset>
#include <memory>

// forwards
class QTimer;
class QSound;

namespace Ipponboard
{
class IView;
class IGoldenScoreView;
class AbstractRules;

///
/// \brief The Controller class references to the state machine (IpponboardSM). IpponboardSM gets a reference back to the controlle for calling methods in this class
///
class Controller : public QObject
    ,private IControllerCore
    ,public IController
{
	Q_OBJECT

public:
    // constructors / destructors
    virtual ~Controller();

    IController const* GetIController() const { return this; }
    IController* GetIController() { return this; }

    void Init();

    static const char* const msg_Ippon;
	static const char* const msg_WazariAwaseteIppon;
	static const char* const msg_Wazaari;
	static const char* const msg_Yuko;
	static const char* const msg_Shido;
	static const char* const msg_Hansokumake;
	static const char* const msg_Osaekomi;
	static const char* const msg_SonoMama;
	//static const char* const msg_Hantei;
	static const char* const msg_Hikiwaki;
	static const char* const msg_Winner;

    // --- IController ---
        // IControllerView
    virtual int GetScore(FighterEnum whos, Score::Point point) const final;
    virtual FighterEnum GetLastHolder() const final { return m_Tori; }
    virtual QString GetCurrentFightFighterLastName(FighterEnum) const final;
    virtual QString GetCurrentFightFighterFirstName(FighterEnum) const final;

        // IControllerMainWindowBase
    virtual void RegisterView(IView* pView) final;
    virtual void RegisterView(IGoldenScoreView* pView) final;
    virtual EState GetCurrentState() const final { return m_State; }
    virtual FighterEnum GetLead() const final;
    virtual QString GetTimeText(ETimer timer) const final;
    virtual void SetTimerValue(ETimer timer, const QString& value) final;
    virtual void SetAutoAdjustPoints(bool isActive) final { m_isAutoAdjustPoints = isActive; }
    virtual void OverrideRoundTimeOfFightMode(int fightTimeSecs) final { m_mode.fightTimeInSeconds = fightTimeSecs; }
    virtual QString const& GetGongFile() const final { return m_gongFile; }
    virtual void SetGongFile(const QString& gong_file) final { m_gongFile = gong_file; }
    virtual void Gong() const final;

        // IControllerAllMainWindows
    virtual void DoAction(EAction action, FighterEnum who = FighterEnum::First, bool doRevoke = false) final;
    virtual void SetRoundTime(const QString& value) final;
    virtual void SetRoundTime(const QTime& time) final ;
    //FIXME: virtual int GetRound() const final { /    return m_currentRound * 10 + m_currentFight + 1; }
    virtual void SetCurrentFightGoldenScore(bool isGS) final;
    virtual bool IsGoldenScore() const final { return is_golden_score(); }
    virtual std::shared_ptr<AbstractRules> GetRules() const final { return m_rules; }
    virtual void SetRules(std::shared_ptr<AbstractRules> rules) final;
    virtual bool IsAutoAdjustPoints() const final { return m_isAutoAdjustPoints; }
    virtual QString GetHomeLabel() const final { return m_labelHome; }
    virtual QString GetGuestLabel() const final { return m_labelGuest; }
    virtual void SetLabels(QString const& home, QString const& guest) final;
    virtual void ClearFightsAndResetTimers() final;

    // other
    void SetFight(unsigned int tournament_index, size_t fight_index,
                  const QString& weight, const QString& first_player_name,
                  const QString& first_player_club, const QString& second_player_name,
                  const QString& second_player_club, int yuko1 = -1,
                  int wazaari1 = -1, int ippon1 = -1, int shido1 = -1,
                  int hansokumake1 = -1, int yuko2 = -1, int wazaari2 = -1,
                  int ippon2 = -1, int shido2 = -1, int hansokumake2 = -1) const;

    void SetCurrentFightFighterName(FighterEnum whos, const QString name);
    QString GetCurrentFightFighterName(FighterEnum who) const;
    QString GetCurrentFightFighterClub(FighterEnum who) const;

protected:
    // constructors / destructors
    Controller();

    // --- IControllerTournament ---
    virtual void InitTournament(TournamentMode const& mode);
    Tournament m_Tournament;
    TournamentMode m_mode;

    virtual QString const GetWeightClass() const final { return m_Tournament[m_currentRound]->at(m_currentFight).GetWeightClass(); }
    //virtual PTableModelTeam GetTournamentScoreModel(int which = 0);
    Fight& get_current_fight(); // TODO rename
    Fight const& get_current_fight() const; // TODO rename

    // other
    void update_views() const;
    IpponboardSM* m_pSM;
    std::shared_ptr<AbstractRules> m_rules;
    EState m_State;
    int m_currentRound;
    int m_currentFight;
    QTime m_roundTime;
    QTime* m_pTimeMain;
    QTime* m_pTimeHold; // needed when side is not chosen yet

private:
    // --- IControllerCore ---
    /* This methods will only be called by the StateMachine. SO make it private and the StateMachine a friend */
    void start_timer(ETimer t) final;
    void stop_timer(ETimer t) final;
    void reset_timer(ETimer) final;
    int get_time(ETimer) const final;
    void save_current_fight() final;
    void reset_current_fight() final;
    Score& get_current_fight_score(FighterEnum who) final;
    Score const& get_current_fight_score(FighterEnum who) const final;
    bool is_sonomama() const final { return m_isSonoMama; }
    bool is_golden_score() const final { return get_current_fight().IsGoldenScore(); }
    bool is_option(EOption option) const final { return GetOption(option); } // TODO: use GetOption!
    bool is_auto_adjust() const final { return IsAutoAdjustPoints(); }

    // other
    QString GetMessage() const { return m_Message; }
    void SetWeightClass(QString const& c);
    QString const& GetCategoryName() const { return m_weight_class; } //TODO: weight class should be part of tournament!
    void SetOption(EOption option, bool isSet);
    bool GetOption(EOption option) const { return m_options.test(option); }
    int GetFightDuration(QString const& weight) const { return m_mode.GetFightDuration(weight); }

    void reset();
    void reset_timer_value(ETimer timer);
    void reset_timers();

private slots:
	void update_main_time();
	void update_hold_time();

private:
   FighterEnum m_Tori;
	std::set<IView*> m_views;
	std::set<IGoldenScoreView*> m_goldenScoreViews;
	QString m_Message;
	QString m_gongFile;
	bool m_isSonoMama;
	QString m_weight_class;
	std::bitset<eOption_MAX> m_options;
    bool m_isAutoAdjustPoints;
	QString m_labelHome;
	QString m_labelGuest;
    QTimer* m_pTimerMain;
    QTimer* m_pTimerHold;
};
} // namespace Ipponboard

#endif  // BASE__CONTROLLER_H_
