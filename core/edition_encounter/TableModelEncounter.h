#ifndef __TABLEMODELENCOUNTER_H_
#define __TABLEMODELENCOUNTER_H_

#include <QAbstractTableModel>

namespace Ipponboard
{
class TableModelEncounter : public QAbstractTableModel
{
	Q_OBJECT
public:
    explicit TableModelEncounter(QObject* parent = 0);
    virtual ~TableModelEncounter();

	void SetNumRows(int rows)
	{
		beginResetModel();
		m_nRows = rows;
		endResetModel();
	}
	size_t GetNumRows() const { return m_nRows; }

    // QAbstractTableModel area (required)
	int rowCount(const QModelIndex& parent) const;
	int columnCount(const QModelIndex& parent) const;
	QVariant data(const QModelIndex& index, int role) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
	Qt::ItemFlags flags(const QModelIndex& index) const;

    // QAbstractTableModel area (optional)
	void SetDataChanged();

private:
    // table view elements
    enum EColumns
    {
        eCol_name1,
        eCol_weight1,
        eCol_name2,
        eCol_weight2,
        eCol_MAX
    };

    size_t m_nRows;
	QString m_HeaderData[eCol_MAX];
	int m_HeaderSizes[eCol_MAX];

    // tournament elements
    //QVector<FightEncounter> Tournament;
};
} // namespace Ipponboard
#endif  // __TABLEMODELENCOUNTER_H_
