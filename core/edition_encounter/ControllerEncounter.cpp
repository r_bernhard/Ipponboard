#include "../util/debug.h"
#include "ControllerEncounter.h"

using namespace Ipponboard;

///////////////////////////////////////////////////////////////////////////////
// Constructor / Destructor
///////////////////////////////////////////////////////////////////////////////
ControllerEncounter::ControllerEncounter()
{
    TRACE(2, "ControllerEncounter::ControllerEncounter()");

    Init();
}

ControllerEncounter::~ControllerEncounter()
{
    TRACE(2, "ControllerEncounter::~ControllerEncounter()");
}

///////////////////////////////////////////////////////////////////////////////
// Overrides
///////////////////////////////////////////////////////////////////////////////

void ControllerEncounter::InitTournament(TournamentMode const& mode)
{
    TRACE(2, "ControllerEncounter::InitTournament()");

    Controller::InitTournament(mode);
}
