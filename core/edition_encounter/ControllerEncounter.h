#ifndef CONTROLLERENCOUNTER_H
#define CONTROLLERENCOUNTER_H

#include "../Controller.h"

namespace Ipponboard
{
class ControllerEncounter : public Controller
{
public:
    // constructors / destructors
    ControllerEncounter();
    virtual ~ControllerEncounter();

protected:
    // --- IControllerTournament ---
    virtual void InitTournament(TournamentMode const& mode) final;
};
} // namespace Ipponboard

#endif // CONTROLLERENCOUNTER_H
