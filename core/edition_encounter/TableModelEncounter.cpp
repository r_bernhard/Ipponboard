﻿#include "../util/debug.h"
#include "TableModelEncounter.h"
#include "../util/helpers.hpp"

#include <QSize>

using namespace Ipponboard;

enum { eDefaultRowHeight = 20 };

//=========================================================
TableModelEncounter::TableModelEncounter(QObject* parent)
//=========================================================
	: QAbstractTableModel(parent)
{
    TRACE(2, "TableModelEncounter::TableModelEncounter()");

    m_HeaderData[eCol_name1] = tr("Firstname Lastname");
    m_HeaderData[eCol_weight1] = tr("Weight [kg]");
    m_HeaderData[eCol_name2] = tr("Firstname Lastname");
    m_HeaderData[eCol_weight2] = tr("Weight [kg]");

    m_HeaderSizes[eCol_name1] = 300;
    m_HeaderSizes[eCol_weight1] = 50;
    m_HeaderSizes[eCol_name2] = 300;
    m_HeaderSizes[eCol_weight2] = 50;
}

//=========================================================
TableModelEncounter::~TableModelEncounter()
//=========================================================
{
    TRACE(2, "TableModelEncounter::~TableModelEncounter()");
}


//=========================================================
int TableModelEncounter::rowCount(const QModelIndex& parent) const
//=========================================================
{
    TRACE(6, "TableModelEncounter::rowCount()");

    return (parent.isValid() && parent.column() != 0) ? 0 : (int)m_nRows;
}

//=========================================================
int TableModelEncounter::columnCount(const QModelIndex& parent) const
//=========================================================
{
    TRACE(6, "TableModelEncounter::columnCount()");

    return parent.isValid() ? 0 : eCol_MAX;
}

//=========================================================
QVariant TableModelEncounter::data(const QModelIndex& index, const int role) const
//=========================================================
{
    TRACE(8, "TableModelEncounter::data()");

	if (!index.isValid())
		return QVariant();

	switch (role)
	{
	case Qt::EditRole:
	case Qt::DisplayRole:
		if (index.row() < (int)m_nRows &&
				index.column() >= 0 &&
				index.column() < eCol_MAX)
		{
			const int row = index.row();

            //const Ipponboard::Fight& fight = m_pTournamentRound->at(row);

			switch (index.column())
			{
            //case eCol_weight:
            //	return fight.weight;

            //case eCol_name1:
            //	return fight.fighters[0].name;

            //case eCol_name2:
            //	return fight.fighters[1].name;

            /*case eCol_time_remaining:
                {
                    // get time display
                    return fight.GetTimeRemainingString();
                }

			case eCol_time:
				{
					// we do get and set the total score display here
					Q_ASSERT(m_pEditWins && m_pEditScore);
					std::pair<unsigned, unsigned> wins = GetTotalWins();
					std::pair<unsigned, unsigned> score = GetTotalScore();

					if (m_pIntermediateModel)
					{
						std::pair<unsigned, unsigned> intermediate_wins = m_pIntermediateModel->GetTotalWins();
						std::pair<unsigned, unsigned> intermediate_score = m_pIntermediateModel->GetTotalScore();

						wins.first += intermediate_wins.first;
						wins.second += intermediate_wins.second;
						score.first += intermediate_score.first;
						score.second += intermediate_score.second;
					}

					m_pEditWins->setText(QString::number(wins.first) + " : " + QString::number(wins.second));
					m_pEditScore->setText(QString::number(score.first) + " : " + QString::number(score.second));

					// get time display
					QString ret = fight.GetTotalTimeElapsedString();

					if (ret == QString("0:00") && !fight.is_saved)
					{
						return QString();
					}

					return ret;
                }

			default:
				break;*/
			}
			return QString();
		}

	case Qt::SizeHintRole:
		{
			if (index.column() < static_cast<int>(sizeof(m_HeaderSizes)) &&
					index.column() > 0)
			{
				return QSize(m_HeaderSizes[index.column()], eDefaultRowHeight);
			}

			break;
		}

	case Qt::TextAlignmentRole:
		{
/*            if (eCol_firstname1 != index.column() && eCol_firstname2 != index.column())
			{
				return Qt::AlignCenter;
            }*/

			break;
		}

	default:
		break;
	}

	return QVariant();
}

//=========================================================
QVariant TableModelEncounter::headerData(const int section, const Qt::Orientation orientation, const int role) const
//=========================================================
{
    TRACE(6, "TableModelEncounter::headerData()");

    if (role == Qt::DisplayRole)
	{
		if (Qt::Vertical == orientation)
			return QString::number(section + 1);

		if (section < fm::ArrayLength(m_HeaderData))
			return m_HeaderData[section];
	}

	return QAbstractItemModel::headerData(section, orientation, role);
}

//=========================================================
bool TableModelEncounter::setData(const QModelIndex& index, const QVariant& value, const int role)
//=========================================================
{
    TRACE(2, "TableModelEncounter::setData()");

    if (!index.isValid() || (flags(index) & Qt::ItemIsEditable) == 0 ||
			role != Qt::EditRole)
	{
		return false;
	}

    constexpr bool result(false);

    // TODO: Fill the list: 1. Type in the values via the list, 2. Save the list, 3. Load the list after startup


    if (index.row() < (int)m_nRows &&
			index.column() >= 0 &&
			index.column() < eCol_MAX)
	{
		const int row = index.row();

/*		Ipponboard::Fight& fight = m_pTournamentRound->at(row);

		switch (index.column())
		{
		case eCol_weight:
			fight.weight = value.toString();
			result = true;
			break;

		case eCol_name1:
			fight.fighters[0].name = value.toString();
			result = true;
			break;

		case eCol_name2:
			fight.fighters[1].name = value.toString();
			result = true;
			break;

		case eCol_time_remaining:
			{
				break;
			}

		case eCol_time:
			{
				result = fight.SetElapsedFromTotalTime(value.toString());
				break;
			}

		default:
			break;
		}
*/
	}

	if (result)
	{
		emit dataChanged(index, index);
	}

	return result;
}

//=========================================================
Qt::ItemFlags TableModelEncounter::flags(const QModelIndex& index) const
//=========================================================
{
    TRACE(6, "TableModelEncounter::flags()");

    if (!index.isValid())
	{
        return Qt::ItemFlags();
	}

	return (Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
}

//=========================================================
void TableModelEncounter::SetDataChanged()
//=========================================================
{
    TRACE(2, "TableModelEncounter::SetDataChanged()");

    const QModelIndex idxStart = this->index(0, 0);
	const QModelIndex idxEnd = this->index(eCol_MAX - 1, (int)m_nRows - 1);

	emit dataChanged(idxStart, idxEnd);
}
