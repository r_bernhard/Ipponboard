// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#include "../util/debug.h"
#include "Fighter.h"

using namespace Ipponboard;

Fighter::Fighter(QString const& firstName,
				 QString const& lastName)
    : m_first_name(firstName)
    , m_last_name(lastName)
    , m_name()
    , m_club()
    , m_weight()
    //, m_category()
{
    TRACE(2, "Fighter::Fighter()");
    m_name = m_first_name + m_last_name;
}
