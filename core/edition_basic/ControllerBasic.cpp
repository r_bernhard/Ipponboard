#include "../util/debug.h"
#include "ControllerBasic.h"

using namespace Ipponboard;

///////////////////////////////////////////////////////////////////////////////
// Constructor / Destructor
///////////////////////////////////////////////////////////////////////////////
ControllerBasic::ControllerBasic()
{
    TRACE(2, "ControllerBasic::ControllerBasic()");

    Init();
}

ControllerBasic::~ControllerBasic()
{
    TRACE(2, "ControllerBasic::~ControllerBasic()");
}

///////////////////////////////////////////////////////////////////////////////
// Overrides
///////////////////////////////////////////////////////////////////////////////

void ControllerBasic::InitTournament(TournamentMode const& mode)
{
    TRACE(2, "ControllerBasic::InitTournament()");

    Controller::InitTournament(mode);
}
