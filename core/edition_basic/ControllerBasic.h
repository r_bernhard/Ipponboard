#ifndef CONTROLLERBASIC_H
#define CONTROLLERBASIC_H

#include "../Controller.h"

namespace Ipponboard
{
class ControllerBasic : public Controller
{
public:
    // constructors / destructors
    ControllerBasic();
    virtual ~ControllerBasic();

protected:
    // --- IControllerTournament ---
    virtual void InitTournament(TournamentMode const& mode) final;
};
} // namespace Ipponboard

#endif // CONTROLLERBASIC_H
