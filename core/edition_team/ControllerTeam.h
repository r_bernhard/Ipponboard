#ifndef CONTROLLERTEAM_H
#define CONTROLLERTEAM_H

#include "../Controller.h"

namespace Ipponboard
{
class ControllerTeam : public Controller
{
public:
    // constructors / destructors
    ControllerTeam();
    virtual ~ControllerTeam();

    virtual int GetTeamScore(FighterEnum who) const final;
    virtual void SetWeights(QStringList const& weights) final;
    virtual int GetCurrentRound() const final { return m_currentRound; }
    virtual int GetRoundCount() const final { return static_cast<int>(m_Tournament.size()); }
    virtual Fight const& GetFight(unsigned int tournament_index, unsigned int fight_index) const final { return m_Tournament[tournament_index]->at(fight_index); }
    virtual int GetCurrentFight() const final { return m_currentFight; }
    virtual int GetFightCount() const final { return static_cast<int>(m_Tournament.at(0)->size()); }
    virtual void NextFight() final;
    virtual void PrevFight() final;
    virtual QString GetFightTimeString() const final { return m_roundTime.toString("m:ss"); } //FIXME: use main time value instead
    virtual void CopyAndSwitchGuestFighters() final;
    virtual void SetClub(FighterEnum whos, const QString& clubName) final;
    void SetCurrentFight(unsigned int index);

    // --- IControllerTournament ---
    virtual void InitTournament(TournamentMode const& mode) final;
    virtual PTableModelTeam GetTournamentScoreModel(int which = 0);

private:
    void SetCurrentRound(unsigned int index);

    std::vector<std::shared_ptr<TableModelTeam> > m_TeamTournamentModels; // TODO move this into ControllerTeam
};
} // namespace Ipponboard

#endif // CONTROLLERTEAM_H
