#include "../util/debug.h"
#include "ControllerTeam.h"

using namespace Ipponboard;

///////////////////////////////////////////////////////////////////////////////
// Constructor / Destructor
///////////////////////////////////////////////////////////////////////////////
ControllerTeam::ControllerTeam() : Controller()
    , m_TeamTournamentModels()
{
    TRACE(2, "ControllerTeam::ControllerTeam()");

    Init();
}

ControllerTeam::~ControllerTeam()
{
    TRACE(2, "ControllerTeam::~ControllerTeam()");
}

///////////////////////////////////////////////////////////////////////////////
// Overrides
///////////////////////////////////////////////////////////////////////////////

void ControllerTeam::InitTournament(TournamentMode const& mode)
{
    TRACE(2, "ControllerTeam::InitTournament()");

    m_TeamTournamentModels.clear();
    m_Tournament.clear();

    m_mode = mode;
    m_rules = RulesFactory::Create(m_mode.rules);
    m_rules->SetCountSubscores(m_mode.IsOptionSet(TournamentMode::str_Option_AllSubscoresCount));

    QStringList actualWeights = m_mode.weights.split(';');

    for (int round = 0; round < m_mode.nRounds; ++round)
    {
        PTournamentRound pRound(new TournamentRound());

        for (int fightNo = 0; fightNo < m_mode.FightsPerRound(); ++fightNo)
        {
            QString weight = m_mode.weightsAreDoubled ?
                                 actualWeights[fightNo / 2] :
                                 actualWeights[fightNo];

            Fight fight { Score(), Score() };
            fight.SetWeightClass(weight);
            fight.SetRoundTime(m_mode.GetFightDuration(weight));
            fight.SetRules(m_rules);
            fight.GetRules()->SetCountSubscores(m_mode.IsOptionSet(TournamentMode::str_Option_AllSubscoresCount));

            const Fighter emptyFighter = Fighter(QString(), QString());
            fight.SetFighter(FighterEnum::First, emptyFighter);
            fight.SetFighter(FighterEnum::Second, emptyFighter);

            pRound->emplace_back(fight);
        }

        m_Tournament.push_back(pRound);

        PTableModelTeam pModel(new TableModelTeam(pRound));
        pModel->SetNumRows(m_mode.FightsPerRound());

        m_TeamTournamentModels.push_back(pModel);
    }

    // set options AFTER configuring fights

    m_currentRound = 0;
    m_currentFight = 0;

    // set time and update views
    SetRoundTime(QTime(0,0,0,0).addSecs(m_mode.GetFightDuration(get_current_fight().GetWeightClass())));
}

PTableModelTeam ControllerTeam::GetTournamentScoreModel(const int which)
{
    TRACE(2, "ControllerTeam::GetTournamentScoreModel()");

    if (((size_t)which) < m_TeamTournamentModels.size() && which > 0)
        return m_TeamTournamentModels[which];

    return m_TeamTournamentModels[0];
}

int ControllerTeam::GetTeamScore(const Ipponboard::FighterEnum who) const
{
    TRACE(2, "ControllerTeam::GetTeamScore()");
    int score(0);

    for (size_t round(0); round < m_Tournament.size(); ++round)
    {
        for (size_t fight(0); fight < m_Tournament[0]->size(); ++fight)
        {
            if (m_Tournament[round]->at(fight).IsSaved())
            {
                score += m_Tournament[round]->at(fight).HasWon(who);
            }
        }
    }

    return score;
}

void ControllerTeam::SetWeights(QStringList const& weights)
{
    TRACE(2, "ControllerTeam::SetWeights()");

    if (weights.count() == GetFightCount())
    {
        for (int round(0); round < GetRoundCount(); ++round)
        {
            for (int fight(0); fight < GetFightCount(); ++fight)
            {
                Fight& f = m_Tournament.at(round)->at(fight);
                f.SetWeightClass(weights.at(fight));
                f.SetRoundTime(m_mode.GetFightDuration(f.GetWeightClass()));
            }
        }
    }
    else
    {
        // duplicate each entry
        for (int round(0); round < GetRoundCount(); ++round)
        {
            for (int fight(0); fight < GetFightCount() - 1; ++fight)
            {
                Fight& f1 = m_Tournament.at(round)->at(fight);
                f1.SetWeightClass(weights.at(fight / 2));
                f1.SetRoundTime(m_mode.GetFightDuration(f1.GetWeightClass()));

                Fight& f2 = m_Tournament.at(round)->at(fight + 1);
                f2.SetWeightClass(weights.at(fight / 2));
                f2.SetRoundTime(m_mode.GetFightDuration(f2.GetWeightClass()));

                ++fight;
            }
        }
    }

    for (auto const & pModel : m_TeamTournamentModels)
    {
        pModel->SetDataChanged();
    }
}

void ControllerTeam::NextFight()
{
    TRACE(2, "ControllerTeam::NextFight()");

    // move to Stopped state
    // (will stop all timers and thus save the current fight)
    m_pSM->process_event(IpponboardSM_::Finish());

    const auto currentFight = GetCurrentFight();
    const auto currentRound = GetCurrentRound();

    if (currentFight == GetFightCount() - 1)
    {
        if (currentRound < GetRoundCount() - 1)
        {
            SetCurrentRound(currentRound + 1);
            SetCurrentFight(0);
        }
    }
    else
    {
        SetCurrentFight(currentFight + 1);
    }
}

void ControllerTeam::PrevFight()
{
    TRACE(2, "ControllerTeam::PrevFight()");

    // move to Stopped state
    // (will stop all timers and thus save the current fight)
    m_pSM->process_event(IpponboardSM_::Finish());

    const auto currentFight = GetCurrentFight();
    const auto currentRound = GetCurrentRound();

    if (currentFight == 0)
    {
        if (currentRound > 0)
        {
            SetCurrentRound(currentRound - 1);
            SetCurrentFight(GetFightCount() - 1);
        }
    }
    else
    {
        SetCurrentFight(currentFight - 1);
    }
}

void ControllerTeam::SetCurrentFight(const unsigned int index)
{
    TRACE(2, "ControllerTeam::SetCurrentFight(index=%u)", index);

    // now set pointer to next fight
    m_currentFight = index;
    *m_pTimeHold = QTime(0,0,0,0);
    m_roundTime = QTime(0,0,0,0).addSecs(m_mode.GetFightDuration(get_current_fight().GetWeightClass()));

    //FIXME: check this block
    if (get_current_fight().IsGoldenScore())
    {
        *m_pTimeMain = QTime(0,0,0,0).addSecs(get_current_fight().GetGoldenScoreTime());
    }
    else
    {
        *m_pTimeMain = QTime(0,0,0,0).addSecs(get_current_fight().GetRemainingTime());
    }

    // update state
    m_State = EState(m_pSM->current_state()[0]);
    Q_ASSERT(eState_TimerStopped == m_State);

    update_views();
}

void ControllerTeam::SetCurrentRound(const unsigned int index)
{
    TRACE(2, "ControllerTeam::SetCurrentRound(index=%u)", index);

    if (index < static_cast<unsigned int>(GetRoundCount()))
    {
        m_currentRound = index;
        update_views();
    }
}

void ControllerTeam::CopyAndSwitchGuestFighters()
{
    TRACE(2, "ControllerTeam::CopyAndSwitchGuestFighters()");

    if (m_Tournament.size() != 2)
    {
        throw std::exception(); // FIXME: use correct exception!
    }

    for (int fight(0); fight < GetFightCount() - 1; ++fight)
    {
        m_Tournament[1]->at(fight).SetFighter(FighterEnum::First, m_Tournament[0]->at(fight).GetFighter(FighterEnum::First));

        m_Tournament[1]->at(fight + 1).SetFighter(FighterEnum::First, m_Tournament[0]->at(fight + 1).GetFighter(FighterEnum::First));

        m_Tournament[1]->at(fight + 1).SetFighter(FighterEnum::Second, m_Tournament[0]->at(fight).GetFighter(FighterEnum::Second));

        m_Tournament[1]->at(fight).SetFighter(FighterEnum::Second, m_Tournament[0]->at(fight + 1).GetFighter(FighterEnum::Second));

        ++fight;
    }

    m_TeamTournamentModels[1]->SetDataChanged();
}

void ControllerTeam::SetClub(const Ipponboard::FighterEnum who, const QString& clubName)
{
    TRACE(2, "ControllerTeam::SetClub(clubName=%s)", clubName.toUtf8().data());

    Q_ASSERT(who == Ipponboard::FighterEnum::First ||
             who == Ipponboard::FighterEnum::Second);

    for (unsigned int round(0); round < m_Tournament.size(); ++round)
    {
        for (size_t fight(0); fight < m_Tournament[0]->size(); ++fight)
        {
            m_Tournament[round]->at(fight).GetFighter(who).SetClub(clubName);
        }
    }

    update_views();
}

///////////////////////////////////////////////////////////////////////////////
// Other
///////////////////////////////////////////////////////////////////////////////

