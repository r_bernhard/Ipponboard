// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef UTIL__PATH_HELPERS_H_
#define UTIL__PATH_HELPERS_H_

#ifdef _WIN32
#	include <Windows.h>
#	include <Shlobj.h>
//#	pragma comment(lib,"Shell32.lib")
#endif

#include "debug.h"

#include <QCoreApplication>
#include <QDir>

namespace fm
{

namespace
{

const QString GetSettingsFilePath(const char* fileName)
{
    TRACE(2, "GetSettingsFilePath(fileName=%s)", fileName);

    // use current application directory
    QString configPath = QCoreApplication::applicationDirPath();
    return QDir::toNativeSeparators(QDir(configPath).filePath(fileName));
}

} // anonymous namespace
} // namespace fmu

#endif  // UTIL__PATH_HELPERS_H_
