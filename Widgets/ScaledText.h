// Copyright 2018 Florian Muecke. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE.txt file.

#ifndef WIDGETS__SCALEDTEXT_H_
#define WIDGETS__SCALEDTEXT_H_

#include <QFont>
#include <QWidget>
#include <QSet>

class QPaintEvent;
class QResizeEvent;
class QTextLayout;

class ScaledText : public QWidget
{
	Q_OBJECT

public:
	explicit ScaledText(QWidget* pParent = 0);
//	explicit ScaledText( const QFont& font, QWidget* pParent = 0 );
    ~ScaledText();

	enum ETextSize
	{
		eSize_normal = 0,
		eSize_uppercase,
		eSize_full,
		eSize_MAX
	};

    void SetText(const QString& text, ETextSize size = eSize_normal, bool rotate = false, short maxLength = 0);
	//> has to be called after setting the font

	void SetFont(const QFont& font);
	//> will call ReDraw() - default setFont() does not!

	void SetFontAndColor(const QFont& font, const QColor& textColor,
						 const QColor& bgColor = Qt::transparent);

	void SetColor(const QColor& textColor);
	void SetColor(const QColor& textColor, const QColor& bgColor);

	const QColor& GetColor() const;
	const QColor& GetBgColor() const;

	ETextSize GetSize() const;

	void SetBlinking(bool blink, int delay = 750);
	bool IsBlinking() const;

	void Redraw();

    void setAlignment(Qt::AlignmentFlag flag);

    void AddAssociatedScaledText(ScaledText* pAssociatedScaledText);
    qreal CalculateZoom() const;

protected:
    void paintEvent(QPaintEvent* event);
    void timerEvent(QTimerEvent* event);

private:
	void set_size(ETextSize size);
	void update_text_metrics();

	QString m_Text;
	QColor m_TextColor;
	QColor m_BGColor;
	Qt::AlignmentFlag m_Alignment;
	QTextLayout* m_pLayout;
	int m_timerId;
	int m_timerDelay;
	ETextSize m_textSize;
	bool m_isVisible;
	bool m_rotate;

    QSet<ScaledText*> m_AssociatedScaledTextSet;
};

#endif  // WIDGETS__SCALEDTEXT_H_
